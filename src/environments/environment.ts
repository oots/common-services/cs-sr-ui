import {EuiEnvConfig} from '@eui/base';

export const environment: EuiEnvConfig = {
  production: false,
  version: '1.4.0',
  enableDevToolRedux: true,
  evidenceTypeClassificationPrefix: 'https://sr.oots.tech.europa.eu/evidencetypeclassifications',
  envDynamicConfig: {
    uri: 'assets/env-json-config.json',
    deepMerge: true,
    merge: ['modules']
  },
  endpoint: {url: 'https://sr.dev.oots.tech.ec.europa.eu/'},
  userAppUrl: 'https://dev.oots-common-services.eu/sr/'
};
