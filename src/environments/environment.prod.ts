import {EuiEnvConfig} from '@eui/base';

export const environment: EuiEnvConfig = {
  production: true,
  version: '1.4.0',
  enableDevToolRedux: false,
  evidenceTypeClassificationPrefix: 'https://sr.oots.tech.europa.eu/evidencetypeclassifications',
  envDynamicConfig: {
    uri: 'assets/env-json-config.json',
    deepMerge: true,
    merge: ['modules'],
  },
  endpoint: {url: ''},
  userAppUrl: ''
};
                                            