import {Injectable} from '@angular/core';
import {I18nService} from '@eui/core';
import {Observable} from 'rxjs';
import {EuiServiceStatus} from '@eui/base';

@Injectable({
  providedIn: 'root',
})
export class AppStarterService {

  constructor(protected i18nService: I18nService) {
  }

  i18nInit(): Observable<EuiServiceStatus> {
    return this.i18nService.init({activeLang: 'en'});
  }
}
