import {Component, OnInit} from '@angular/core';
import {AppStarterService} from "./app-starter.service";
import {BaseComponent} from "./core/components/base/base.component";
import {takeUntil} from "rxjs/operators";
import {SharedService} from "@shared/service/shared.service";

@Component({
  selector: 'app-root',
  template: '<app-layout></app-layout>',
})
export class AppComponent extends BaseComponent implements OnInit {
  constructor(
    private readonly appStarterService: AppStarterService,
    private readonly sharedService: SharedService,
  ) {
    super();
  }

  ngOnInit() {
    this.appStarterService.i18nInit().pipe(takeUntil(this.destroyed)).subscribe(() => {
    })
   this.sharedService.fetchMasterData();
  }
}
