import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { NotFoundComponent } from "./core/components/not-found/not-found.component";
import { customMatcher } from "./core/matchers/url-decoding.matcher";

const ROUTES: Routes = [
  { path: "", redirectTo: "home", pathMatch: "full" },
  {
    path: "home",
    loadChildren: () =>
      import("@features/home/home.module").then((m) => m.HomeModule),
  },
  {
    path: "search",
    loadChildren: () =>
      import("@features/search/search.module").then((m) => m.SearchModule),
  },
  {
    matcher: customMatcher,
    loadChildren: () =>
      import("@features/asset/asset.module").then((m) => m.AssetModule),
  },
  { path: "not-found", component: NotFoundComponent },
  { path: "**", redirectTo: "not-found", pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
