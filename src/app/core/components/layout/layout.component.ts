import {Component} from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrl: './layout.component.scss',
})
export class LayoutComponent {
  mainMenuItems = [
    {label: 'app.components.menu.button.home', url: './home'},
    {label: 'app.components.menu.button.semanticRepository', url: './search'}
  ]
}
