import {Subject} from 'rxjs';
import {Component, OnDestroy} from '@angular/core';

@Component({
  selector: 'app-base',
  template:''
})
export class BaseComponent implements OnDestroy {
  destroyed = new Subject<void>()

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
  }
}
