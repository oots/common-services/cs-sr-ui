import { UrlMatchResult, UrlSegment } from "@angular/router";

export function customMatcher(segments: UrlSegment[]): UrlMatchResult {
  if (segments?.[0]?.path === "asset") {
    return {
      consumed: segments,
      posParams: {
        id: new UrlSegment(
          segments
            .slice(1)
            .map((segment) => segment.path)
            .join("/"),
          {
            id: segments
              .slice(1)
              .map((segment) => segment.path)
              .join("/"),
          },
        ),
      },
    };
  }
  return null;
}
