import {Pipe, PipeTransform} from '@angular/core';
import {LocaleModel} from "@shared/models/sr.model";

@Pipe({
  name: 'localeLanguage',
})
export class LocaleLanguagePipe implements PipeTransform {

  transform(list: LocaleModel[], key: string, activeLanguage: string): string {
    return list.find(locale => locale.language.code.toLocaleLowerCase() === activeLanguage.toLowerCase())?.[key];
  }

}
