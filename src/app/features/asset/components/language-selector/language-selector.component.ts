import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {EuiPopoverComponent} from '@eui/components/eui-popover';
import {LocaleModel} from "@shared/models/sr.model";
import {LanguageModel} from "@shared/models/master-data.model";
import {ENGLISH_LANGUAGE} from "@features/asset/constants/language.const";

@Component({
  selector: 'app-language-selector',
  templateUrl: './language-selector.component.html',
  styleUrl: './language-selector.component.scss'
})
export class LanguageSelectorComponent {
  @Input() locales: LocaleModel[] = [];
  @Output() languageChanged = new EventEmitter<LanguageModel>();
  @ViewChild('popover') popover: EuiPopoverComponent;
  selectedLanguage: LanguageModel = ENGLISH_LANGUAGE;
  ENGLISH_LANGUAGE = ENGLISH_LANGUAGE;

  openPopover(event) {
    if(!this.locales?.length) return;
    this.popover.openPopover(event.target);
  }

  onLanguageChange(language: LanguageModel) {
    this.selectedLanguage = language;
    this.languageChanged.emit(language)
  }
}
