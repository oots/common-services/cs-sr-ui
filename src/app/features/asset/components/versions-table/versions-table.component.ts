import { Component, EventEmitter, Input, Output } from "@angular/core";
import { VersionModel } from "@features/asset/components/versions-table/version.model";
import { AssetService } from "@features/asset/service/asset.service";

@Component({
  selector: "app-versions-table",
  templateUrl: "./versions-table.component.html",
  styleUrl: "./versions-table.component.scss",
})
export class VersionsTableComponent {
  @Input() set versionData(data: VersionModel[]) {
    this._versionData = data;
    this.activeVersion = this.getActiveVersion(data)
  }

  @Output() showVersionButtonClicked: EventEmitter<VersionModel> =
    new EventEmitter<VersionModel>();
  activeVersion: string = "";
  _versionData: VersionModel[] = [];

  constructor(private readonly assetService: AssetService) {
  }

  onShowVersionButtonClicked(event: VersionModel) {
    this.activeVersion = event.version;
    this.showVersionButtonClicked.emit(event);
  }

  private getActiveVersion(data: VersionModel[]): string {
    /*
    * With  hardcodedUrlVersion we cover the case of getting the value from url directly
    * */
    const hardcodedUrlVersion = window.location.href.split("/").at(-1);
    const isHardcodedUrlVersion = data.map((v: VersionModel) => v.version).includes(hardcodedUrlVersion)
    return isHardcodedUrlVersion ? hardcodedUrlVersion : this.assetService.getAssetVersion() || data?.find(version => !!version.isCurrentVersion)?.version
  }
}
