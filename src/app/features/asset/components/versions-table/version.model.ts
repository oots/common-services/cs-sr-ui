export interface VersionModel {
  version?: string;
  releaseNotes?: string;
  issuedDate?: string;
  isCurrentVersion?: boolean;
  downloadUrl?: string;
}

