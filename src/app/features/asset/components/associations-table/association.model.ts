import {AssociationTypeEnum} from "@shared/enums/association-type.enum";

export interface AssociationTableModel {
  type?: AssociationTypeEnum;
  name: string;
  description?: string;
  identifier: string;  // used for redirection (redirection link onclick)
  version?: string;
  showSearchButton?: boolean;
}
