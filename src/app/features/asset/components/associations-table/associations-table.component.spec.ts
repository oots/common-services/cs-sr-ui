import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssociationsTableComponent } from './associations-table.component';

describe('AssociationsTableComponent', () => {
  let component: AssociationsTableComponent;
  let fixture: ComponentFixture<AssociationsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AssociationsTableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AssociationsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
