import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AssociationTableModel } from "@features/asset/components/associations-table/association.model";
import { AssociationTypeEnum } from "@shared/enums/association-type.enum";

@Component({
  selector: 'app-associations-table',
  templateUrl: './associations-table.component.html',
  styleUrl: './associations-table.component.scss'
})
export class AssociationsTableComponent {
  @Input() associationsData: AssociationTableModel[];
  @Output() associationSelected = new EventEmitter<AssociationTableModel>();
  @Output() findAssetsClicked = new EventEmitter<AssociationTableModel>();

  onSelectAssociation(asset: AssociationTableModel) {
    if (asset.type === AssociationTypeEnum.IS_PROFILE_OF || asset.type === AssociationTypeEnum.IS_SNAPSHOT_OF) {
      this.openExternalAsset(asset.identifier);
    } else {
      this.associationSelected.emit(asset);
    }
  }

  onFindAssetsClicked(asset: AssociationTableModel) {
    this.findAssetsClicked.emit(asset);
  }

  private openExternalAsset(url: string) {
    window.open(url, '_blank');
  }
}