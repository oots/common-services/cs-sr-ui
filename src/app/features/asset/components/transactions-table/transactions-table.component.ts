import { Component, Input, Output, EventEmitter } from '@angular/core';
import { TransactionModel } from "@shared/models/sr.model";

@Component({
  selector: 'app-transactions-table',
  templateUrl: './transactions-table.component.html',
  styleUrl: './transactions-table.component.scss'
})
export class TransactionsTableComponent {
  @Input({required: true}) transactionsData!: TransactionModel[];
  @Output() findAssetsClicked = new EventEmitter<TransactionModel>();

  onFindAssetsClicked(asset: TransactionModel) {
    this.findAssetsClicked.emit(asset);
  }
}
