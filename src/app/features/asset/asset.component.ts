import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { BaseComponent } from "../../core/components/base/base.component";
import { AssetService } from "@features/asset/service/asset.service";
import { DownloadPopupTranslationsModel } from "@shared/components/download-popup/download-popup-translations.model";
import { DownloadPopupComponent } from "@shared/components/download-popup/download-popup.component";
import { LanguageModel } from "@shared/models/master-data.model";
import { StatusEnum } from "@shared/enums/status.enum";
import { VersionModel } from "@features/asset/components/versions-table/version.model";
import { AssociationTableModel } from "@features/asset/components/associations-table/association.model";
import { Router } from "@angular/router";
import { ENVIRONMENTAL_URL } from "@shared/constants/environmental-url.const";
import { map } from "rxjs";
import { RedirectionDataEnum } from "@shared/enums/redirection.enum";
import { SrFilterEnum } from "@features/search/enums/sr.enum";
import { MapperEnum } from "@shared/enums/mapper.enum";

@Component({
  selector: "app-asset",
  templateUrl: "./asset.component.html",
  styleUrl: "./asset.component.scss",
})
export class AssetComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild("popup") popup: DownloadPopupComponent;

  popupTranslations: DownloadPopupTranslationsModel = {
    title: "app.features.asset.details.download.popup.title",
    content: "app.features.asset.details.download.popup.content",
    cancelButton: "app.features.asset.details.download.popup.cancelButton",
    downloadButton: "app.features.asset.details.download.popup.downloadButton",
    checkboxLabel: "app.features.asset.details.download.popup.checkboxLabel",
  };
  activeLanguage = this.assetService.getActiveLanguage();
  StatusEnum = StatusEnum;
  selectedAsset$ = this.assetService.selectedAsset$
    .pipe(
      map(asset => ({
        ...asset,
        assetTypeCode: this.assetService.keyMap(asset.type, SrFilterEnum.ASSET_TYPE, MapperEnum.DESCRIPTION, MapperEnum.CODE)
      })));
  versions$ = this.assetService.versions$
  assetGroupType: 'sds' | 'tdd' | null = null;
  RedirectionDataEnum = RedirectionDataEnum;

  constructor(private readonly assetService: AssetService, private readonly router: Router) {
    super();
  }

  ngOnInit() {
    this.checkMasterData();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.assetService.resetState();
  }

  copyTextToClipboard(uri: string) {
    navigator.clipboard.writeText(uri);
  }

  onDownloadButtonClick() {
    this.popup.onDialogOpen();
  }

  onLanguageChanged(language: LanguageModel) {
    this.assetService.setActiveLanguage(language);
  }

  onDownloadAssetXml(identifier: string, filename: string, version: string) {
    const url = `${identifier}${version ? '?version=' + version : ''}`;
    const filenameWithVersion = `${filename}${version ? ' v' + version : ''}`;
    this.assetService.downloadXmlVersion(url, filenameWithVersion);
  }

  onShowVersionButtonClicked(event: VersionModel, identifier: string) {
    this.router.navigate(["/asset", ...identifier.replace(ENVIRONMENTAL_URL, "").split("/")], {queryParams: {version: event.version}});
  }

  onSelectAssociation(event: AssociationTableModel) {
    const routeParts = ["/asset", ...event.identifier.split("/")];
    this.router.navigate(routeParts, {queryParams: {version: event.version}});
  }

  onDistributionDownloadButtonClicked(distributionResourceId: number) {
    this.assetService.downloadDistribution(distributionResourceId);
  }

  onFindAssetsClicked(data, type: RedirectionDataEnum) {
    this.assetService.setRedirectionData({data, type});
    this.router.navigate(["/search"]);
  }

  openExternalLink(url: string) {
    window.open(url, '_blank');
  }

  private checkMasterData() {
    this.assetService.checkMasterData();
  }
}
