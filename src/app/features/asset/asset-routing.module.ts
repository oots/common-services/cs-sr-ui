import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AssetComponent} from "@features/asset/asset.component";
import {assetDetailsResolver} from "@features/asset/resolvers/asset-details.resolver";

const routes: Routes = [{
  path: '',
  component: AssetComponent,
  runGuardsAndResolvers: "always",
  resolve: [assetDetailsResolver]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssetRoutingModule {
}
