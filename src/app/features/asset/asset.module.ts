import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from "@shared/shared.module";
import {AssetRoutingModule} from "@features/asset/asset-routing.module";
import {AssetComponent} from "@features/asset/asset.component";
import {LanguageSelectorComponent} from "@features/asset/components/language-selector/language-selector.component";
import {VersionsTableComponent} from './components/versions-table/versions-table.component';
import {LocaleLanguagePipe} from "@features/asset/pipes/locale-language.pipe";
import {TransactionsTableComponent} from "@features/asset/components/transactions-table/transactions-table.component";
import {AssociationsTableComponent} from "@features/asset/components/associations-table/associations-table.component";

@NgModule({
  declarations: [
    AssetComponent,
    LanguageSelectorComponent,
    VersionsTableComponent,
    LocaleLanguagePipe,
    TransactionsTableComponent,
    AssociationsTableComponent
  ],
  imports: [
    SharedModule,
    AssetRoutingModule,
    CommonModule
  ]
})
export class AssetModule {
}
