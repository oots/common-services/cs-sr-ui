export const ENGLISH_LANGUAGE = {
  code: 'en',
  name: 'English',
  codeEu: 'ENG'
}
