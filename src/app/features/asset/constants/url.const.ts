import {ENVIRONMENTAL_URL} from "@shared/constants/environmental-url.const";

export const URL_GET_ASSET_DETAILS = (id: string, version: string = '') => `${ENVIRONMENTAL_URL}assets/asset-details?identifier=${ENVIRONMENTAL_URL}${id}${version ? '&version=' + version : ''}`;
export const URL_POST_ASSET_SEARCH: string = `${ENVIRONMENTAL_URL}assets/search`;
export const URL_GET_DOWNLOAD_DISTRIBUTION_FILE = (distributionResourceId: number) => `${ENVIRONMENTAL_URL}distributions-metadata/download?distributionResourceId=${distributionResourceId}`;
export const URL_GET_VERSION_XML = (identifier: string, version: string) => `${identifier}?version=${version}`;
