import { Injectable } from "@angular/core";
import { SharedService } from "@shared/service/shared.service";
import {
  AssetModel,
  RedirectionDataModel,
  SearchResponseModel,
  TableDistributionsModel,
  TransactionModel
} from "@shared/models/sr.model";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import {
  URL_GET_ASSET_DETAILS,
  URL_GET_DOWNLOAD_DISTRIBUTION_FILE,
  URL_GET_VERSION_XML,
  URL_POST_ASSET_SEARCH,
} from "@features/asset/constants/url.const";
import { BehaviorSubject, combineLatestWith, map, Observable, tap } from "rxjs";
import { SrFilterEnum } from "@features/search/enums/sr.enum";
import { MapperEnum } from "@shared/enums/mapper.enum";
import { VersionModel } from "@features/asset/components/versions-table/version.model";
import { AssociationTableModel } from "@features/asset/components/associations-table/association.model";
import { AssociationTypeEnum } from "@shared/enums/association-type.enum";
import { LanguageModel } from "@shared/models/master-data.model";
import { ENGLISH_LANGUAGE } from "@features/asset/constants/language.const";

@Injectable({
  providedIn: "root",
})
export class AssetService {
  selectedAsset = new BehaviorSubject<AssetModel>(null);
  activeLanguage = new BehaviorSubject<LanguageModel>(ENGLISH_LANGUAGE);
  selectedAsset$ = this.selectedAsset.asObservable()
    .pipe(
      combineLatestWith(
        this.activeLanguage.asObservable()),
      map(([asset]) =>
        this.mapResponse(asset)),
      tap(asset => {
        if (this.updateVersions) {
          this.getVersions(asset?.identifier)
        }
      }),
    );

  versions = new BehaviorSubject<VersionModel[]>([]);
  versions$ = this.versions.asObservable();
  private assetVersion: string = null;
  private updateVersions = true;

  constructor(
    private readonly sharedService: SharedService,
    private readonly http: HttpClient,
  ) {
  }

  getAssetDetails(id: string, version?: string): Observable<AssetModel> {
    if (version !== undefined)
      this.setAssetVersion(version);
    return this.http
      .get<AssetModel>(URL_GET_ASSET_DETAILS(encodeURI(id), version))
  }

  getVersions(id: string) {
    const body = {
      filter: [
        {
          fieldName: "identifier",
          fieldValues: [id],
        },
        {
          fieldName: SrFilterEnum.SHOW_VERSIONS,
          fieldValues: ['true'],
        },
      ],
      sort: [
        {
          fieldName: SrFilterEnum.VERSION,
          order: "desc",
        },
      ],
    };
    this.http.post<SearchResponseModel>(URL_POST_ASSET_SEARCH, body).subscribe({
        next: (response: SearchResponseModel) => {
          this.sharedService.setError(null);
          const versionsData: VersionModel[] = []
          response.list.forEach((asset) => {
              if (asset.version) {
                versionsData.push({
                    version: asset.version,
                    releaseNotes: this.mapStringToMarkup(asset.versionNotes),
                    issuedDate: asset.issued,
                    isCurrentVersion: asset?.isCurrentVersion,
                    downloadUrl: URL_GET_VERSION_XML(asset.identifier, asset.version),
                  }
                )
              }
            }
          )
          this.versions.next(versionsData);
        },
        error: (err: HttpErrorResponse) => {
          this.sharedService.setError(err.status);
        }
      }
    );
  }

  resetState() {
    this.sharedService.setError(null);
    this.activeLanguage.next(ENGLISH_LANGUAGE);
    this.setAssetVersion(null);
  }

  downloadXmlVersion(url: string, filename: string = "sr") {
    this.http.get(url, {responseType: "text"}).subscribe({
      next: (response) => {
        this.sharedService.downloadFile(
          response,
          "text/xml",
          `${filename}.xml`,
        );
      },
      error: (err: HttpErrorResponse) => {
        this.sharedService.setError(err.status);
      }
    });
  }

  setSelectedAsset(asset) {
    this.selectedAsset.next(asset);
  }

  keyMap(
    value: string,
    filter: string | SrFilterEnum,
    key: MapperEnum,
    target: MapperEnum,
  ) {
    return this.sharedService.keyMap(value, filter, key, target);
  }

  checkMasterData() {
    if (!this.sharedService.getMasterData()) {
      this.sharedService.fetchMasterData();
    }
  }

  downloadDistribution(distributionResourceId: number) {
    this.sharedService.downloadDistribution(
      URL_GET_DOWNLOAD_DISTRIBUTION_FILE(distributionResourceId),
    );
  }

  setAssetVersion(version: string) {
    this.assetVersion = version;
  }

  getAssetVersion() {
    return this.assetVersion;
  }

  setActiveLanguage(language: LanguageModel) {
    this.activeLanguage.next(language);
  }

  getActiveLanguage() {
    return this.activeLanguage.value;
  }

  setRedirectionData(data: RedirectionDataModel) {
    this.sharedService.setRedirectionData(data)
  }

  private mapAssociationData(asset: AssetModel): AssociationTableModel[] {
    // change return data if isProfileOf exists also add a flag   to show.hide the buttons
    const associationData: AssociationTableModel[] = [];

    asset?.isPartOf?.forEach(value => {
      associationData.push({
        type: AssociationTypeEnum.IS_PART_OF,
        version: value.version,
        name: value.title,
        identifier: value.identifier.substring(1),
        description: value.description,
        showSearchButton: true
      })
    })

    asset?.conformsTo?.forEach(value => {
      associationData.push({
        type: AssociationTypeEnum.CONFORMS_TO,
        version: value.version,
        name: value.title,
        identifier: value.identifier.substring(1),
        description: value.description,
        showSearchButton: true
      })
    })

    asset?.isProfileOf?.forEach(value => {
      associationData.push({
        type: AssociationTypeEnum.IS_PROFILE_OF,
        name: value.title,
        version: value.version || null,
        identifier: value.landingPage,
        description: value.description,
        showSearchButton: false
      })
    })

    asset?.isSnapshotOf?.forEach(value => {
      associationData.push({
        type: AssociationTypeEnum.IS_SNAPSHOT_OF,
        name: value.title,
        version: value.version || null,
        identifier: value.landingPage,
        description: value.description,
        showSearchButton: false
      })
    })
    return associationData || null;
  }

  private handleDistributions(asset: AssetModel): TableDistributionsModel[] {
    return this.sharedService.handleDistributions(asset, this.activeLanguage.value);
  }

  private handleTransactions(asset: AssetModel): TransactionModel[] {
    return asset.transactions?.map(transaction => ({
      ...transaction,
      title: this.activeLanguage && transaction.locales.find(locale => locale.language.code === this.activeLanguage.value.code)?.title || transaction.title,
      type: this.keyMap(transaction.type, SrFilterEnum.TRANSACTION_TYPE, MapperEnum.CODE, MapperEnum.DESCRIPTION),
      subject: this.keyMap(transaction.subject, SrFilterEnum.TRANSACTION_SUBJECT, MapperEnum.CODE, MapperEnum.DESCRIPTION)
    })) || null
  }

  private mapResponse(asset: AssetModel) {
    return {
      ...asset,
      title: this.activeLanguage && asset.locales.find(locale => locale.language.code === this.activeLanguage.value.code)?.title || asset.title,
      description: this.activeLanguage && asset.locales.find(locale => locale.language.code === this.activeLanguage.value.code)?.description || asset.description,
      distributions: this.handleDistributions(asset),
      referenceFrameworks: asset.referenceFrameworks.map(fw => fw.identifier),
      status: this.keyMap(
        asset.status,
        SrFilterEnum.STATUS,
        MapperEnum.CODE,
        MapperEnum.DESCRIPTION,
      ),
      type: this.keyMap(
        asset.type,
        SrFilterEnum.ASSET_TYPE,
        MapperEnum.CODE,
        MapperEnum.DESCRIPTION,
      ),
      transactions: this.handleTransactions(asset),
      associationsData: this.mapAssociationData(asset),
      themes: asset.themes.map(theme => this.keyMap(theme, SrFilterEnum.THEME, MapperEnum.CODE, MapperEnum.DESCRIPTION)),
    }
  }

  private mapStringToMarkup(notes: string): string {
    const urlRegex = /\bhttps?:\/\/[^\s/$.?#].[^\s]*\b/g;
    if (notes?.match(urlRegex))
      return notes.replace(urlRegex, (url) => `<a href="${url}">${url}</a>`);
    return notes
  }
}
