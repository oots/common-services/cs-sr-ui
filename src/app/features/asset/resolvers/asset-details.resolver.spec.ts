import { TestBed } from '@angular/core/testing';
import { ResolveFn } from '@angular/router';

import { assetDetailsResolver } from './asset-details.resolver';

describe('assetDetailsResolver', () => {
  const executeResolver: ResolveFn<boolean> = (...resolverParameters) => 
      TestBed.runInInjectionContext(() => assetDetailsResolver(...resolverParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeResolver).toBeTruthy();
  });
});
