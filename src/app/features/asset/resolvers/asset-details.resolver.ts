import { inject } from "@angular/core";
import { ResolveFn, Router } from "@angular/router";
import { AssetService } from "@features/asset/service/asset.service";
import { AssetModel } from "@shared/models/sr.model";
import { catchError, EMPTY, tap } from "rxjs";

export const assetDetailsResolver: ResolveFn<AssetModel> = (route) => {
  const router = inject(Router);
  const assetService = inject(AssetService);
  const state = router.getCurrentNavigation().extras.state;
  let id: string;
  let version: string;

  if (state?.versionedIdentifier) {
    id = state?.versionedIdentifier;
    version = undefined;
    assetService.setAssetVersion(state.version || state?.versionedIdentifier.split("/").at(-1))
  }

  if (route.queryParams["version"]) {
    id = route.paramMap.get("id");
    version = id?.includes(route.queryParams["version"]) ? null : route.queryParams["version"]
  }

  if (id?.includes('distributions')) {
    id = id.replace("distributions", "datamodels");
  }
  if (!id && !version) {
    id = route.paramMap.get("id")
  }

  return assetService
    .getAssetDetails(id, version)
    .pipe(
      tap((asset: AssetModel) => assetService.setSelectedAsset(asset)),
      catchError(() => {
        router.navigate(["not-found"]);
        return EMPTY;
      })
    );
}