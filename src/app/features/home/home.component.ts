import {Component} from '@angular/core';
import {environment} from "../../../environments/environment";

@Component({
  templateUrl: './home.component.html',
  selector: 'app-home',
  styleUrl: 'home.component.scss'
})
export class HomeComponent {
  searchLink: string = '/search';
  version: string = environment?.version.toString();
}
