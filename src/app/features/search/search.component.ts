import {Component, OnInit} from '@angular/core';
import {SrService} from "@features/search/service/sr.service";
import {ColumnEnum, ColumnIdEnum} from "@shared/enums/data-table.enum";
import {map} from "rxjs";
import {DataTableConfigModel} from "@shared/models/data-table.model";
import {BREADCRUMBS_CONFIG} from "@shared/constants/breadcrumbs.const";

@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrl: "./search.component.scss",
})
export class SearchComponent implements OnInit {
  tableData$ = this.srService.tableData$;
  paginationOptions$ = this.srService.tableOptions$.pipe(
    map((options) => ({
      pageSize: options.pageSize,
      page: options.pageNumber,
    })),
  );
  tableConfig: DataTableConfigModel = {
    isExpandable: true,
    pagination: {
      pageSizeOptions: [5, 10, 25, 50],
      visibilityLimit: 25,
    },
    asyncTable: true,
    isResponsive: true,
    isHoverable: true,
    translations: null,
    defaultSorting: {
      columns: ColumnIdEnum.MODIFIED,
      order: "desc",
    },
  };

  columns = [
    {
      id: ColumnIdEnum.ASSET_TYPE,
      type: ColumnEnum.TEXT,
      columnLabel: "app.features.sr.assetsList.table.columnTitle.assetType",
      isSortable: true,
      isVisible: true,
    },
    {
      id: ColumnIdEnum.ASSET_NAME,
      type: ColumnEnum.LINK,
      columnLabel: "app.features.sr.assetsList.table.columnTitle.assetName",
      isSortable: true,
      isVisible: true,
    },
    {
      id: ColumnIdEnum.VERSION,
      type: ColumnEnum.CHIP,
      columnLabel: "app.features.sr.assetsList.table.columnTitle.version",
      isSortable: true,
      isVisible: true,
    },
    {
      id: ColumnIdEnum.PUBLISHER,
      type: ColumnEnum.TEXT,
      columnLabel: "app.features.sr.assetsList.table.columnTitle.publisher",
      isSortable: true,
      isVisible: true,
    },
    {
      id: ColumnIdEnum.MODIFIED,
      type: ColumnEnum.TEXT,
      columnLabel: "app.features.sr.assetsList.table.columnTitle.lastModified",
      isSortable: true,
      isVisible: true,
    },
    {
      id: ColumnIdEnum.STATUS,
      type: ColumnEnum.TEXT,
      columnLabel: "app.features.sr.assetsList.table.columnTitle.status",
      isSortable: true,
      isVisible: false,
    },
    {
      id: ColumnIdEnum.ISSUED,
      type: ColumnEnum.TEXT,
      columnLabel: "app.features.sr.assetsList.table.columnTitle.issueDate",
      isSortable: true,
      isVisible: false,
    },
    {
      id: ColumnIdEnum.THEME,
      type: ColumnEnum.CHIP_ARRAY,
      columnLabel: "app.features.sr.assetsList.table.columnTitle.theme",
      isSortable: false,
      isVisible: false,
    },
    {
      id: ColumnIdEnum.TRANSACTION_COMPONENT,
      type: ColumnEnum.CHIP_ARRAY,
      columnLabel: "app.features.sr.assetsList.table.columnTitle.transactionComponent",
      isSortable: false,
      isVisible: false,
    },
    {
      id: ColumnIdEnum.TRANSACTION_TYPE,
      type: ColumnEnum.CHIP_ARRAY,
      columnLabel: "app.features.sr.assetsList.table.columnTitle.transactionType",
      isSortable: false,
      isVisible: false,
    },
    {
      id: ColumnIdEnum.TRANSACTION_SUBJECT,
      type: ColumnEnum.CHIP_ARRAY,
      columnLabel: "app.features.sr.assetsList.table.columnTitle.transactionSubject",
      isSortable: false,
      isVisible: false,
    },
    {
      id: ColumnIdEnum.ASSET_REPOSITORY,
      type: ColumnEnum.TEXT,
      columnLabel: "app.features.sr.assetsList.table.columnTitle.assetRepository",
      isSortable: true,
      isVisible: false,
    },
  ];
  breadcrumbsConfig = BREADCRUMBS_CONFIG;
  constructor(
    private readonly srService: SrService) {
  }

  ngOnInit() {
    this.srService.fetchUniqueVersions();
  }
}
