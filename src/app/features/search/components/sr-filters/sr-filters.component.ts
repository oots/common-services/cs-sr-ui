import { Component, OnInit, ViewChild } from "@angular/core";
import { DropdownDataModel } from "@shared/components/dropdown/dropdown.model";
import { SrService } from "@features/search/service/sr.service";
import { BaseComponent } from "../../../../core/components/base/base.component";
import { FILTERS_CONFIG, TRANSLATIONS, } from "@features/search/components/sr-filters/sr-filters.config";
import { AssetTypeEnum, SrFilterEnum } from "@features/search/enums/sr.enum";
import { DateRangeModel } from "@shared/components/date-range/date-range.model";
import { EuiTreeSelectionChanges } from "@eui/components/eui-tree";
import { takeUntil } from "rxjs";
import { ActiveFilterModel } from "@shared/models/sr.model";
import { TextInputComponent } from "@shared/components/text-input/text-input.component";
import { AssociationTypeEnum } from "@shared/enums/association-type.enum";
import { MapperEnum } from "@shared/enums/mapper.enum";
import { RepositoryModel } from "@shared/models/master-data.model";

@Component({
  selector: "app-sr-filters",
  templateUrl: "./sr-filters.component.html",
  styleUrl: "./sr-filters.component.scss",
})
export class SrFiltersComponent extends BaseComponent implements OnInit {
  @ViewChild("text") text: TextInputComponent;
  @ViewChild("version") version: TextInputComponent;
  assetTypeData: DropdownDataModel = {data: null};
  assetRepositoryData: DropdownDataModel = {data: null};
  publisherData: DropdownDataModel = {data: null};
  creatorData: DropdownDataModel = {data: null};
  languageData: DropdownDataModel = {data: null};
  mediaTypeData: DropdownDataModel = {data: null};
  statusData: DropdownDataModel = {data: null};
  subjectData: DropdownDataModel = {data: null};
  themeData: DropdownDataModel = {data: null};
  associationTypeData: DropdownDataModel = {data: null};
  transactionTypeData: DropdownDataModel = {data: null};
  transactionComponentData: DropdownDataModel = {data: null};
  defaultModifiedDate = null;
  defaultIssuedDate = null;
  isVersioningDisabled: boolean = false;
  isAssociationFiltersDisabled: boolean = false;
  config = FILTERS_CONFIG;
  translations = TRANSLATIONS;
  SrFilterEnum = SrFilterEnum;
  masterData$ = this.srService.masterData$;
  uniqueVersions$ = this.srService.uniqueVersions$;
  assetUriList$ = this.srService.assetUriList$;
  associatedAssetVersions$ = this.srService.associatedAssetVersions$
  keywordValue = "";
  versionValue = "";
  assetUriValue = "";
  associatedVersion: string = "";
  showAssociatedVersions = false;
  associationCheckboxFlag = false;
  hideComponentTypeSubjectFilters = false;
  hideThemeFilter = false;
  isAssetVersionDisabled = true;

  constructor(private readonly srService: SrService) {
    super();
  }

  ngOnInit() {
    if (this.srService.getRedirectionData()) {
      this.srService.searchForRelatedAssets();
      this.isAssetVersionDisabled = false;
    } else {
      this.setFiltersData();
    }
    this.subscribeToTableOptionChanges();
  }

  onFilterChange(event, filter: SrFilterEnum) {
    switch (filter) {
      case SrFilterEnum.SHOW_VERSIONS:
        this.handleCheckbox(event);
        break;
      case SrFilterEnum.VERSION:
      case SrFilterEnum.ASSOCIATION_URI:
      case SrFilterEnum.ASSOCIATED_VERSION:
      case SrFilterEnum.KEYWORD:
        this.handleText(event, filter);
        break;
      case SrFilterEnum.MODIFIED:
      case SrFilterEnum.ISSUED:
        this.handleDates(event, filter);
        break;
      case SrFilterEnum.PUBLISHER:
      case SrFilterEnum.CREATOR:
        this.handleAgents(event, filter);
        break;
      case SrFilterEnum.SHOW_ASSOCIATIONS:
      case SrFilterEnum.ASSOCIATION_TYPE:
        this.handleAssociations(event);
        break;
      default:
        this.handleDefaultCase(event, filter);
        break;
    }
  }

  onAssociationCheckboxClicked(value: boolean) {
    if (value) {
      this.onFilterChange(value, SrFilterEnum.SHOW_ASSOCIATIONS)
    } else {
      this.srService.removeFilterGroup(SrFilterEnum.ASSOCIATION_TYPE, false);
      this.srService.removeFilterGroup(SrFilterEnum.ASSOCIATION_URI, false);
      this.srService.removeFilterGroup(SrFilterEnum.ASSOCIATED_VERSION);
    }
    this.associationCheckboxFlag = !value;
  }

  private setFiltersData(activeFilters: ActiveFilterModel[] = []) {
    if (!this.srService.getMasterData()) {
      this.masterData$.pipe(takeUntil(this.destroyed)).subscribe(masterData => {
        this.setData(masterData, [])
      })
      return;
    }
    this.setData(this.srService.getMasterData(), activeFilters)
    this.srService.setRedirectionData(null)
  }

  private setData(masterData, activeFilters) {
    // DROPDOWNS
    this.assetTypeData = {
      ...this.assetTypeData,
      disabledDataLabels: this.getDisabledDataList(activeFilters),
      data: masterData?.assetTypes?.map((type) => ({
        label: type.description,
        isVisible: activeFilters.length ? this.checkIfVisible(activeFilters, SrFilterEnum.ASSET_TYPE, type.code) : false,
      }))
    }
    this.publisherData = {
      ...this.publisherData, data: masterData?.agents?.map((agent) => ({
        label: agent.name,
        isVisible: activeFilters.length ? this.checkIfVisible(activeFilters, SrFilterEnum.PUBLISHER, agent.name) : false
      }))
    }
    this.creatorData = {
      ...this.creatorData, data: masterData?.agents?.map((agent) => ({
        label: agent.name,
        isVisible: activeFilters.length ? this.checkIfVisible(activeFilters, SrFilterEnum.CREATOR, agent.name) : false
      }))
    }
    this.languageData = {
      ...this.languageData, data: masterData?.languages?.map((language) => ({
        label: language.name,
        isVisible: activeFilters.length ? this.checkIfVisible(activeFilters, SrFilterEnum.LANGUAGE, language.code) : false,
      }))
    }
    this.mediaTypeData = {
      ...this.mediaTypeData, data: masterData?.mediaTypes?.map((type) => ({
        label: type.description,
        isVisible: activeFilters.length ? this.checkIfVisible(activeFilters, SrFilterEnum.MEDIA_TYPE, type.code) : false
      }))
    }
    this.statusData = {
      ...this.statusData, data: masterData?.statuses?.map((status) => ({
        label: status.description,
        isVisible: activeFilters.length ? this.checkIfVisible(activeFilters, SrFilterEnum.STATUS, status.code) : false
      }))
    }
    this.themeData = {
      ...this.themeData, data: masterData?.themes?.map((theme) => ({
        label: theme.description,
        isVisible: activeFilters.length ? this.checkIfVisible(activeFilters, SrFilterEnum.THEME, theme.code) : false
      }))
    }
    this.transactionTypeData = {
      ...this.transactionTypeData, data: masterData?.transactionTypes?.map((transactionType) => ({
        label: transactionType.description,
        isVisible: activeFilters.length ? this.checkIfVisible(activeFilters, SrFilterEnum.TRANSACTION_TYPE, transactionType.code) : false
      }))
    }
    this.transactionComponentData = {
      ...this.transactionComponentData, data: masterData?.transactionComponents?.map((transactionComponent) => ({
        label: transactionComponent.description,
        isVisible: activeFilters.length ? this.checkIfVisible(activeFilters, SrFilterEnum.TRANSACTION_COMPONENT, transactionComponent.code) : false
      }))
    }
    this.associationTypeData = {
      ...this.associationTypeData, data: masterData?.associationTypes?.map((associationType) => ({
        label: associationType.description,
        isVisible: activeFilters.length ? this.checkIfVisible(activeFilters, SrFilterEnum.ASSOCIATION_TYPE, associationType.code) : false
      }))
    }
    this.subjectData = {
      ...this.subjectData, data: masterData?.transactionSubjects?.map((subject) => ({
        label: subject.description,
        isVisible: activeFilters.length ? this.checkIfVisible(activeFilters, SrFilterEnum.TRANSACTION_SUBJECT, subject.code) : false
      }))
    }
    this.assetRepositoryData = {
      ...this.assetRepositoryData,
      data: masterData?.assetRepositories?.map((assetRepository: RepositoryModel) => ({
        label: assetRepository.code,
        isVisible: activeFilters.length ? this.checkIfVisible(activeFilters, SrFilterEnum.ASSET_REPOSITORY, assetRepository.code) : false,
      }))
    }

    // TEXT INPUT
    this.keywordValue = activeFilters.find(filter => filter.filter === SrFilterEnum.KEYWORD)?.value.toString() || ""
    this.versionValue = activeFilters.find(filter => filter.filter === SrFilterEnum.VERSION)?.value.toString() || ""
    this.assetUriValue = activeFilters.find(filter => filter.filter === SrFilterEnum.ASSOCIATION_URI)?.value.toString() || ""
    this.associatedVersion = activeFilters.find(filter => filter.filter === SrFilterEnum.ASSOCIATED_VERSION)?.value.toString() || ""

    // DATE RANGE
    this.defaultModifiedDate = activeFilters?.find(filter => filter.filter === SrFilterEnum.MODIFIED)?.value.toString() || "";
    this.defaultIssuedDate = activeFilters?.find(filter => filter.filter === SrFilterEnum.ISSUED)?.value.toString() || "";

    // SHOW VERSIONS CHECKBOX
    this.isVersioningDisabled = activeFilters.find(filter => filter.filter === SrFilterEnum.SHOW_VERSIONS)
    this.isAssociationFiltersDisabled = activeFilters.map(f => f.filter).some(v => [SrFilterEnum.ASSOCIATED_VERSION, SrFilterEnum.ASSOCIATION_TYPE, SrFilterEnum.ASSOCIATION_URI].includes(v)) || this.associationCheckboxFlag

    this.showAssociatedVersions = !!this.assetUriValue;
    this.checkFiltersVisibility(activeFilters);
  }

  private checkIfVisible(activeFilters: ActiveFilterModel[], filterName: SrFilterEnum, value: string): boolean {
    return activeFilters.map(filter => `${filter.filter}-${filter.value}`).includes(`${filterName}-${value}`)
  }

  private checkFiltersVisibility(activeFilters: ActiveFilterModel[]) {
    const assetTypeFilters = activeFilters.filter(f => f.filter === SrFilterEnum.ASSET_TYPE)?.map(filter => filter.value) || [];
    this.hideThemeFilter = activeFilters.some(f => f.filter === SrFilterEnum.ASSET_TYPE) && !assetTypeFilters?.includes(AssetTypeEnum.SCHEMATRON.toUpperCase())
    this.hideComponentTypeSubjectFilters = activeFilters.some(f => f.filter === SrFilterEnum.ASSET_TYPE) && !(assetTypeFilters.includes(AssetTypeEnum.SCHEMATRON.toUpperCase()) || assetTypeFilters.includes(AssetTypeEnum.EXAMPLE.toUpperCase()))
  }

  private handleCheckbox(value: boolean) {
    if (value) {
      this.srService.updateFilters(
        {
          fieldName: SrFilterEnum.SHOW_VERSIONS,
          fieldValues: [true],
        },
        true,
      );
    } else {
      // keep sequence in order to delete the version and issued filters from the request filters
      this.srService.removeFilterGroup(SrFilterEnum.VERSION, false);
      this.srService.removeFilterGroup(SrFilterEnum.ISSUED, false);
      this.srService.removeFilterGroup(SrFilterEnum.SHOW_VERSIONS);
      this.defaultIssuedDate = '';
    }
  }

  private handleText(event, filter: SrFilterEnum, update = true) {
    if (filter === SrFilterEnum.ASSOCIATION_URI) {
      this.showAssociatedVersions = !!event;
    }
    if (!event) {
      this.srService.removeFilterGroup(filter, update);
      if (filter === SrFilterEnum.ASSOCIATION_URI) {
        this.srService.removeFilterGroup(SrFilterEnum.ASSOCIATED_VERSION, update);
      }
      return;
    }
    this.srService.updateFilters(
      {
        fieldName: filter,
        fieldValues: [event],
      },
      true,
    );
  }

  private handleDates(event, filter: SrFilterEnum) {
    if (!event?.firstDate && !event?.lastDate) {
      this.srService.removeFilterGroup(filter);
      return;
    }
    this.srService.updateFilters(
      {
        fieldName: filter,
        fieldValues: this.handleMomentDate(event),
      },
      true,
    );
  }

  private handleDefaultCase(event, filter: SrFilterEnum) {
    if (!event.selection?.length) {
      this.srService.removeFilterGroup(filter);
      return;
    }

    if (event?.removed?.length) {
      this.srService.removeFilter(
        filter,
        event.removed[0].node.treeContentBlock.label,
      );
      return;
    }
    const fieldValues = this.srService.mapFilterValueToRequest(
      filter,
      this.mapToStringArray(event),
    )

    this.srService.updateFilters({
      fieldName: filter,
      fieldValues
    });
  }

  private handleAgents(event, filter: SrFilterEnum) {
    if (event?.removed?.length) {
      this.srService.removeFilterGroup(filter);
      return;
    }

    this.srService.updateFilters(
      {
        fieldName: filter,
        fieldValues: [event.added[0].node.treeContentBlock.label],
      },
      true,
    );
  }

  private handleMomentDate(date: DateRangeModel) {
    const firstDate = date?.firstDate?.format("YYYYMMDD");
    const lastDate = date?.lastDate?.format("YYYYMMDD");

    if (date?.firstDate === date?.lastDate && date?.firstDate) {
      return [firstDate];
    }
    if (
      date?.firstDate &&
      date?.lastDate &&
      date?.firstDate !== date?.lastDate
    ) {
      return [`${firstDate}-${lastDate}`];
    }
    if (date?.firstDate && !date?.lastDate) {
      return [`>${firstDate}`];
    }
    if (!date?.firstDate && date?.lastDate) {
      return [`<${lastDate}`];
    }
    return [];
  }

  private mapToStringArray(input: EuiTreeSelectionChanges): string[] {
    return input.selection.map((data) => data.node.treeContentBlock.label);
  }

  private subscribeToTableOptionChanges() {
    this.srService.activeFilters$.pipe(
      takeUntil(this.destroyed)
    ).subscribe({
      next: (activeFilers) => {
        this.setFiltersData(activeFilers);
      },
    });
  }

  private handleAssociations(event) {
    // change comes from the checkbox(event == boolean) we only call fetchAssetUriList
    if (typeof event === 'boolean') {
      this.srService.fetchAssetUriList([AssociationTypeEnum.CONFORMS_TO, AssociationTypeEnum.IS_PART_OF]);
      return;
    }
    this.fetchAssetUriList(event);
    this.handleText('', SrFilterEnum.ASSOCIATION_URI, false);
    this.handleDefaultCase(event, SrFilterEnum.ASSOCIATION_TYPE);
  }

  private fetchAssetUriList(event) {
    this.srService.fetchAssetUriList(
      event.selection.length === 1
        ? [this.srService.keyMap(event.selection[0].node.treeContentBlock.label, SrFilterEnum.ASSOCIATION_TYPE, MapperEnum.DESCRIPTION, MapperEnum.CODE)]
        : [AssociationTypeEnum.CONFORMS_TO, AssociationTypeEnum.IS_PART_OF])
  }

  private getDisabledDataList(activeFilters: ActiveFilterModel[]) {
    const activeFilterRepositories: string[] = activeFilters.filter(item => item.filter === SrFilterEnum.ASSET_REPOSITORY)?.map(items => items.value) as string[];
    let assetTypesToKeep = [];
    this.srService.getMasterData()?.assetRepositories?.forEach(repo => {
      if (activeFilterRepositories.includes(repo.code)) {
        assetTypesToKeep.push(repo.assetTypes)
      }
    })
    assetTypesToKeep = [...new Set(assetTypesToKeep.flat())];
    return assetTypesToKeep.length ? this.srService.getMasterData()?.assetTypes.map(type => type.description)?.filter(item => !assetTypesToKeep.includes(item)) : [];
  }
}