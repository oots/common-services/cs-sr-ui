import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SrFiltersComponent } from './sr-filters.component';

xdescribe('SrFiltersComponent', () => {
  let component: SrFiltersComponent;
  let fixture: ComponentFixture<SrFiltersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SrFiltersComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SrFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
