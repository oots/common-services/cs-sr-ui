import {DropdownConfigModel} from "@shared/components/dropdown/dropdown.model";

export const TRANSLATIONS = {
  assetTypeTranslations: {
    label: "app.features.sr.filters.label.assetType",
    placeholder: "app.features.sr.filters.placeholder.assetType",
    activeFiltersLabel: "app.components.dropdown.activeFilter",
  },
  publisherTranslations: {
    label: "app.features.sr.filters.label.publisher",
    placeholder: "app.features.sr.filters.placeholder.publisher",
    activeFiltersLabel: "app.components.dropdown.activeFilter",
  },
  creatorTranslations: {
    label: "app.features.sr.filters.label.creator",
    placeholder: "app.features.sr.filters.placeholder.creator",
    activeFiltersLabel: "app.components.dropdown.activeFilter",
  },
  languageTranslations: {
    label: "app.features.sr.filters.label.language",
    placeholder: "app.features.sr.filters.placeholder.language",
    activeFiltersLabel: "app.components.dropdown.activeFilter",
  },
  mediaTypeTranslations: {
    label: "app.features.sr.filters.label.mediaType",
    placeholder: "app.features.sr.filters.placeholder.mediaType",
    activeFiltersLabel: "app.components.dropdown.activeFilter",
  },
  statusTranslations: {
    label: "app.features.sr.filters.label.status",
    placeholder: "app.features.sr.filters.placeholder.status",
    activeFiltersLabel: "app.components.dropdown.activeFilter",
  },
  lastModificationDateRangeTranslations: {
    label: 'app.features.sr.filters.label.lastModification',
    startDatePlaceholder: 'app.features.sr.filters.placeholder.startDate',
    endDatePlaceholder: 'app.features.sr.filters.placeholder.endDate',
  },
  versionIssueDateRangeTranslations: {
    label: 'app.features.sr.filters.label.versionIssueDate',
    startDatePlaceholder: 'app.features.sr.filters.placeholder.startDate',
    endDatePlaceholder: 'app.features.sr.filters.placeholder.endDate',
  },
  keywordTranslations: {
    label: "app.features.sr.filters.label.keyword",
    placeholder: "app.features.sr.filters.placeholder.keyword",
  },
  versionIdentifierTranslations: {
    label: "app.features.sr.filters.label.versionIdentifier",
    placeholder: "app.features.sr.filters.placeholder.versionIdentifier",
  },
  transactionTypeTranslations: {
    label: "app.features.sr.filters.label.transactionType",
    placeholder: "app.features.sr.filters.placeholder.transactionType",
    activeFiltersLabel: "app.components.dropdown.activeFilter",
  },
  transactionComponentTranslations: {
    label: "app.features.sr.filters.label.transactionComponent",
    placeholder: "app.features.sr.filters.placeholder.transactionComponent",
    activeFiltersLabel: "app.components.dropdown.activeFilter",
  },
  themeTranslations: {
    label: "app.features.sr.filters.label.theme",
    placeholder: "app.features.sr.filters.placeholder.theme",
    activeFiltersLabel: "app.components.dropdown.activeFilter",
  },
  associationTypeTranslations: {
    label: "app.features.sr.filters.label.associationType",
    placeholder: "app.features.sr.filters.placeholder.associationType",
    activeFiltersLabel: "app.components.dropdown.activeFilter",
  },
  subjectTranslations: {
    label: "app.features.sr.filters.label.subject",
    placeholder: "app.features.sr.filters.placeholder.subject",
    activeFiltersLabel: "app.components.dropdown.activeFilter",
  },
  repositoryTranslations: {
    label: "app.features.sr.filters.label.assetRepository",
    placeholder: "app.features.sr.filters.placeholder.assetRepository",
    activeFiltersLabel: "app.components.dropdown.activeFilter",
  },
}

export const FILTERS_CONFIG = {
  dropdownMultiSelectConfig: {
    isMultiselect: true,
    euiColor: 'secondary',
    display: "block",
    isRequired: false,
    showTotalInLabel: true
  } as DropdownConfigModel,
  dateRangeConfig: {
    minDate: new Date(),
    maxDate: new Date(),
    isClearable: true,
  },
  dropdownSingleSelectConfig: {
    isMultiselect: false,
    euiColor: 'secondary',
    display: "block",
    isRequired: false,
    showTotalInLabel: true
  } as DropdownConfigModel,
  textInputConfig: {isClearable: false},
  searchInputConfig: {isClearable: false, isSearchInput: true},
}
