import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SrTableHeaderComponent } from './sr-table-header.component';

xdescribe('SrTableHeaderComponent', () => {
  let component: SrTableHeaderComponent;
  let fixture: ComponentFixture<SrTableHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SrTableHeaderComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SrTableHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
