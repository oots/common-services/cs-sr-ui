import {Component, EventEmitter, Input, OnInit, Output, ViewChild,} from "@angular/core";
import {SrService} from "@features/search/service/sr.service";
import {DropdownConfigModel, DropdownDataModel, DropdownTranslationsModel} from "@shared/components/dropdown/dropdown.model";
import {EuiTreeSelectionChanges} from "@eui/components/eui-tree";
import {BaseComponent} from "../../../../../core/components/base/base.component";
import {ColumnModel} from "@shared/models/data-table.model";
import {map} from "rxjs";
import {EuiChip} from "@eui/components/eui-chip";
import {DownloadPopupTranslationsModel} from "@shared/components/download-popup/download-popup-translations.model";
import {DownloadPopupComponent} from "@shared/components/download-popup/download-popup.component";
import {SrFilterEnum} from "@features/search/enums/sr.enum";
import {ColumnIdEnum} from "@shared/enums/data-table.enum";

@Component({
  selector: "app-sr-table-header",
  templateUrl: "./sr-table-header.component.html",
  styleUrl: "./sr-table-header.component.scss",
})
export class SrTableHeaderComponent extends BaseComponent implements OnInit {
  @Input() set dropdownData(dropdownData: ColumnModel[]) {
    this._dropdownData = {
      data: dropdownData
        .filter(data => (data.id !== ColumnIdEnum.ASSET_TYPE && data.id !== ColumnIdEnum.ASSET_NAME))
        .map((data) => ({
          id: data.id,
          label: data.columnLabel,
          isVisible: data.isVisible,
        }))
    }
  }

  @Input() count: number = 0;
  @Output() columnSelectionChanged: EventEmitter<EuiTreeSelectionChanges> =
    new EventEmitter<EuiTreeSelectionChanges>();

  @ViewChild("popup") popup: DownloadPopupComponent;
  dontShowAgain: boolean = false;
  dropdownConfig: DropdownConfigModel = {
    isMultiselect: true,
    display: "flex",
    showTotalInLabel: false,
    euiColor: "primary",
  };
  dropdownTranslations: DropdownTranslationsModel = {
    placeholder: "app.features.sr.assetsList.tableHeader.filterColumns",
  };
  chipsList$ = this.srService.activeFilters$.pipe(
    map((chips) => {
      const chipsList: EuiChip[] = [];
      chips.forEach((filter) =>
        chipsList.push(
          new EuiChip({
            id: `${filter.index}-${filter.filter}`,
            label: this.mapValues(filter.filter, filter.value?.toString()),
            isRemovable: true,
          }),
        ),
      );
      return chipsList
        .filter(
          (chip) =>
            //remove checkbox from filters
            chip.id.toString().split("-")[1] !== SrFilterEnum.SHOW_VERSIONS,
        )
        .sort(
          (a, b) =>
            +a.id.toString().split("-")[0] - +b.id?.toString().split("-")[0],
        );
    }),
  );
  _dropdownData: DropdownDataModel = null;
  popupTranslations: DownloadPopupTranslationsModel = {
    title:
      "app.features.sr.assetsList.tableHeader.downloadAllResults.dialog.title",
    content:
      "app.features.sr.assetsList.tableHeader.downloadAllResults.dialog.content",
    cancelButton:
      "app.features.sr.assetsList.tableHeader.downloadAllResults.dialog.footer.cancel",
    downloadButton:
      "app.features.sr.assetsList.tableHeader.downloadAllResults.dialog.footer.download",
    checkboxLabel:
      "app.features.sr.assetsList.tableHeader.downloadAllResults.dialog.footer.checkboxLabel",
  };

  constructor(private readonly srService: SrService) {
    super();
  }

  ngOnInit() {
    this.checkSessionStorage();
  }

  onColumnSelectionChange(event: EuiTreeSelectionChanges) {
    this.columnSelectionChanged.emit(event);
  }

  onDownloadButtonClick() {
    this.popup.onDialogOpen();
  }

  onChipRemove(chip: EuiChip) {
    const filter = chip.id?.toString().split("-")[1];
    this.srService.removeFilter(filter, chip.label);
  }

  onChipRemoveAll() {
    this.srService.removeAllFilter();
  }

  onFinalDownloadClicked() {
    this.srService.downloadAllSearchResults();
  }

  private checkSessionStorage() {
    this.dontShowAgain = !!sessionStorage.getItem("dontShowAgain");
  }

  private mapValues(
    filter: SrFilterEnum | string,
    value: string,
    isChipRemove = false,
  ) {
    return this.srService.mapValues(filter, value, isChipRemove);
  }
}
