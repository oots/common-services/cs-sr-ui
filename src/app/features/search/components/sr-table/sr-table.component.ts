import { AfterViewInit, Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { ColumnModel, DataTableConfigModel, } from "@shared/models/data-table.model";
import { AssetModel, TableDataModel } from "@shared/models/sr.model";
import { SrService } from "@features/search/service/sr.service";
import { BaseComponent } from "../../../../core/components/base/base.component";
import { ColumnIdEnum } from "@shared/enums/data-table.enum";
import { EuiTreeSelectionChanges } from "@eui/components/eui-tree";
import { EuiPaginatorComponent } from "@eui/components/eui-paginator";
import { Router } from "@angular/router";
import { SortEvent } from "@eui/components/eui-table";
import { Observable, tap } from "rxjs";
import { ENVIRONMENTAL_URL } from "@shared/constants/environmental-url.const";

@Component({
  selector: "app-sr-table",
  templateUrl: "./sr-table.component.html",
  styleUrl: "./sr-table.component.scss",
})
export class SrTableComponent extends BaseComponent implements AfterViewInit {
  @Input() set tableData(tableData: TableDataModel) {
    this._tableData = tableData;
    this.showExpandableHeader = tableData?.list?.some(
      (data) => !!data?.distributions?.length,
    );
  }

  @Input() paginationOptions: { pageSize: number; page: number };

  @Input() set columns(columns: ColumnModel[]) {
    columns.forEach(column => {
      column.isVisible = this.srService.getVisibleColumns()[column.id]
    });
    this._columns = columns;
  }

  @Input() config: DataTableConfigModel = null;
  @Output() chipRemovedAll: EventEmitter<void> = new EventEmitter<void>();
  @ViewChild('paginator') paginator: EuiPaginatorComponent;
  showExpandableHeader = false;
  _columns: ColumnModel[] = [];
  _tableData: TableDataModel = null;
  ColumnIdEnum = ColumnIdEnum;
  showColumns = this.srService.getVisibleColumns();
  genericError$: Observable<number> = this.srService.genericError$.pipe(
    tap(status => {
      if (status) {
        this.errorTitle = `app.features.sr.assetsList.alert.${status}.title`
        this.errorContent = `app.features.sr.assetsList.alert.${status}.content`
      }
    })
  );
  isFirstRun$ = this.srService.isFirstRun$;
  errorTitle: string;
  errorContent: string;

  constructor(
    private readonly srService: SrService,
    private readonly router: Router,
  ) {
    super();
  }

  ngAfterViewInit(): void {
    this.paginator.getPage(this.paginationOptions.page);
  }

  onPageChange(event) {
    if (!event.nbPage) {
      return;
    }
    this.srService.updatePagination(event.page, event.pageSize);
  }

  onColumnSelectionChanged(event: EuiTreeSelectionChanges) {
    const {headers, visibleColumns} = this.srService.setColumnVisibility(event, this._columns);
    this._columns = headers;
    this.showColumns = visibleColumns;
  }

  redirectToAssetDetails(asset: AssetModel) {
    const uri = (asset.versionedIdentifier || asset?.identifier)?.toString().replace(ENVIRONMENTAL_URL, "");
    const state = {
      id: asset.identifier?.replace(ENVIRONMENTAL_URL, ""),
      version: asset.version,
      versionedIdentifier: asset.versionedIdentifier?.replace(ENVIRONMENTAL_URL, "")
    }
    const routeParts = ["/asset", ...uri.split("/")];
    const queryParams = state.versionedIdentifier ? null : {version: asset.version}
    this.router.navigate(routeParts, {queryParams, state});
  }

  onSortChange(event: SortEvent) {
    if (!event.sort && event.order) {
      return;
    }
    this.srService.updateSorting(event);
  }

  onDistributionDownloadButtonClicked(distributionResourceId: number) {
    this.srService.downloadDistribution(distributionResourceId);
  }
}
