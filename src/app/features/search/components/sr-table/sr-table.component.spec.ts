import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SrTableComponent } from './sr-table.component';

xdescribe('SrTableComponent', () => {
  let component: SrTableComponent;
  let fixture: ComponentFixture<SrTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SrTableComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SrTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
