import {NgModule} from "@angular/core";
import {SearchComponent} from "@features/search/search.component";
import {SharedModule} from "@shared/shared.module";
import {SearchRoutingModule} from "@features/search/search-routing.module";
import {SrTableComponent} from "@features/search/components/sr-table/sr-table.component";
import {SrFiltersComponent} from "@features/search/components/sr-filters/sr-filters.component";
import {SrTableHeaderComponent} from "./components/sr-table/sr-table-header/sr-table-header.component";
import {HttpClientModule} from "@angular/common/http";
import {SortPipe} from "@features/search/pipes/sort/sort.pipe";

const DECLARATIONS = [
  SrTableComponent,
  SrFiltersComponent,
  SearchComponent,
  SrTableHeaderComponent,
  SortPipe,
];

@NgModule({
  declarations: [...DECLARATIONS],
  imports: [SharedModule, HttpClientModule, SearchRoutingModule],
  exports: [SrTableComponent],
})
export class SearchModule {
}
