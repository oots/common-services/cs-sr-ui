import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'sort',
})
export class SortPipe implements PipeTransform {

  transform(arr, comparator:string, order: 'asc' | 'desc' = 'asc') {
    if (order === 'asc') {
      return [...arr.sort((a, b) => a[comparator] - b[comparator])];
    }
    return [...arr.sort((a, b) => b[comparator] - a[comparator])];
  }
}

