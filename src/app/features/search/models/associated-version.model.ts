import {AssociationTypeEnum} from "@shared/enums/association-type.enum";

export interface AssociatedVersionModel {
  associationType: AssociationTypeEnum;
  version: string;
  identifier: string;
}
