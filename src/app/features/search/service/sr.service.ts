import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { BehaviorSubject, combineLatestWith, map, Observable } from "rxjs";
import {
  ActiveFilterModel,
  AssetModel,
  FilterModel,
  RedirectionDataModel,
  SearchResponseModel,
  TableDataListModel,
  TableDataModel,
  TableOptionsModel,
  TransactionModel,
} from "@shared/models/sr.model";
import {
  URL_GET_UNIQUE_VERSIONS,
  URL_POST_ALL_SEARCH_RESULTS,
  URL_POST_ASSET_SEARCH,
  URL_POST_UNIQUE_URIS,
} from "@features/search/service/url.const";
import { SrFilterEnum } from "@features/search/enums/sr.enum";
import { SortEvent } from "@eui/components/eui-table";
import { MasterDataResponseModel } from "@shared/models/master-data.model";
import { SharedService } from "@shared/service/shared.service";
import { MapperEnum } from "@shared/enums/mapper.enum";
import { URL_GET_DOWNLOAD_DISTRIBUTION_FILE } from "@features/asset/constants/url.const";
import { EuiTreeSelectionChanges } from "@eui/components/eui-tree";
import { ColumnModel } from "@shared/models/data-table.model";
import { AssociatedVersionModel } from "@features/search/models/associated-version.model";
import { AssociationTypeEnum } from "@shared/enums/association-type.enum";
import { AssociationTableModel } from "@features/asset/components/associations-table/association.model";
import { ENVIRONMENTAL_URL } from "@shared/constants/environmental-url.const";
import { RedirectionDataEnum } from "@shared/enums/redirection.enum";

@Injectable({
  providedIn: "root",
})
export class SrService {
  protected tableData = new BehaviorSubject<TableDataModel | null>({
    list: [],
    count: 0,
  });
  protected tableOptions = new BehaviorSubject<TableOptionsModel | null>({
    pageNumber: 0,
    pageSize: 25,
    filter: [],
    sort: [],
  });
  protected isFirstRun = new BehaviorSubject<boolean>(true);
  protected activeFilters = new BehaviorSubject<ActiveFilterModel[]>([]);
  protected uniqueVersions = new BehaviorSubject<string[]>([]);
  protected associatedAssetVersions = new BehaviorSubject<AssociatedVersionModel[]>([]);
  protected assetUriList = new BehaviorSubject<string[]>([]);
  protected visibleColumns = {
    assetType: true,
    assetName: true,
    publisher: true,
    creator: true,
    modified: true,
    status: false,
    version: true,
    issued: false,
  };
  masterData$: Observable<MasterDataResponseModel> = this.sharedService.masterData$;
  tableData$: Observable<TableDataModel> = this.tableData.asObservable();
  tableOptions$: Observable<TableOptionsModel> = this.tableOptions.asObservable()
  genericError$: Observable<number> = this.sharedService.genericError$;
  isFirstRun$: Observable<boolean> = this.isFirstRun.asObservable();
  activeFilters$: Observable<ActiveFilterModel[]> = this.activeFilters.asObservable();
  uniqueVersions$: Observable<string[]> = this.uniqueVersions.asObservable();
  assetUriList$: Observable<string[]> = this.assetUriList.asObservable();
  associatedAssetVersions$: Observable<string[]> = this.associatedAssetVersions
    .pipe(
      combineLatestWith(this.tableOptions$),
      map(([list, options]) => this.getAssociationVersionsList(list, options))
    )
  private filtersCounter = 0;

  constructor(
    private readonly http: HttpClient,
    private readonly sharedService: SharedService,
  ) {
  }

  fetchUniqueVersions() {
    this.sharedService.setError(null);
    this.http.get(URL_GET_UNIQUE_VERSIONS)
      .subscribe({
          next: (response: string[]) => {
            this.uniqueVersions.next(response);
          },
          error: (err: HttpErrorResponse) => {
            this.sharedService.setError(err.status);
          },
        }
      );
  }

  fetchAssetUriList(filters: string[]) {
    const body = {
      filter: [
        {
          fieldName: "associationType",
          fieldValues: filters
        }
      ]
    }
    this.http
      .post(
        URL_POST_UNIQUE_URIS,
        body,
      )
      .subscribe({
        next: (response: {
          id: number;
          identifier: string;
          title: string;
          version: string;
          associationType: string;
        }[]) => {
          this.assetUriList.next([...new Set(response.map(value => value?.identifier))].sort());
          this.setAssociatedAssetVersions(response);
        },
        error: (err: HttpErrorResponse) => {
          this.sharedService.setError(err.status);
        },
      });
  }

  getVisibleColumns() {
    return this.visibleColumns;
  }

  setColumnVisibility(event: EuiTreeSelectionChanges, columns: ColumnModel[]) {
    const headers: ColumnModel[] = [...columns];
    const selection: string[] = event.selection.map(
      (s) => s.node.treeContentBlock.id,
    );
    selection.push("assetType", "assetName")
    columns.forEach((col, index) => {
      headers[index].isVisible = selection.includes(col.id);
      this.visibleColumns[col.id] = selection.includes(col.id);
    });

    return {headers, visibleColumns: this.visibleColumns};
  }

  removeFilterGroup(filterName: SrFilterEnum, update = true) {
    this.activeFilters.next(
      this.activeFilters.getValue().filter((f) => f.filter !== filterName),
    );
    if (update) this.updateTableOptions();
  }

  removeFilter(filter: SrFilterEnum | string, value: string) {
    let activeFilters = [...this.activeFilters.getValue()];

    if (filter === SrFilterEnum.ASSOCIATION_URI) {
      activeFilters = [...activeFilters].filter(f => f.filter !== SrFilterEnum.ASSOCIATED_VERSION && f.filter !== SrFilterEnum.ASSOCIATION_URI)
    } else {
      activeFilters.splice(
        activeFilters.findIndex(
          (activeFilter) =>
            activeFilter.filter === filter &&
            this.mapValues(filter, value, true) === activeFilter.value,
        ),
        1,
      );
    }
    this.activeFilters.next(activeFilters);
    this.updateTableOptions();
  }

  removeAllFilter() {
    this.activeFilters.next([]);
    this.updateTableOptions();
  }

  updateFilters(filter: FilterModel, singleValue: boolean = false) {
    const activeFilters = this.activeFilters.getValue();
    const filters: ActiveFilterModel[] = [];
    if (singleValue) {
      this.removeFilterGroup(filter.fieldName, false);
      const filters = [...this.activeFilters.getValue()];
      filters.push({
        filter: filter.fieldName,
        value: filter.fieldValues[0],
        index: this.filtersCounter++,
      });
      this.activeFilters.next(filters);
      this.updateTableOptions();
      return;
    }

    filter.fieldValues.forEach((value) => {
      if (!activeFilters.find((activeFilter) => activeFilter.value === value)) {
        filters.push({
          filter: filter.fieldName,
          value: value,
          index: this.filtersCounter++,
        });
      }
    });
    this.activeFilters.next([...activeFilters].concat(filters));
    this.updateTableOptions();
  }

  updatePagination(page: number, size: number) {
    this.tableOptions.next({
      ...this.tableOptions.getValue(),
      pageNumber: page,
      pageSize: size,
    });
    this.assetsSearch();
  }

  updateSorting(sorting: SortEvent) {
    if (
      sorting.sort === this.tableOptions.getValue().sort[0]?.fieldName &&
      sorting.order === this.tableOptions.getValue().sort[0]?.order) {
      return;
    }

    const sortField = sorting.sort === "assetName" ? "title" : sorting.sort;
    this.tableOptions.next({
      ...this.tableOptions.getValue(),
      sort: Object.values(sorting).some((value) => !value)
        ? []
        : [{fieldName: sortField, order: sorting.order}],
      pageNumber: 0,
    });
    this.assetsSearch();
  }

  mapFilterValueToRequest(filterName: SrFilterEnum, data: string[]): string[] {
    let key: string;
    switch (filterName) {
      case SrFilterEnum.STATUS:
        key = 'statuses'
        break;
      case SrFilterEnum.ASSET_REPOSITORY:
        key = 'assetRepositories'
        break;
      default:
        key = filterName.concat('s')
    }
    return data.map(
      (data) =>
        this.sharedService
          .getMasterData()[key]?.find(
          (masterData) => {
            let key;
            switch (filterName) {
              case SrFilterEnum.LANGUAGE:
                key = MapperEnum.NAME
                break;
              case SrFilterEnum.ASSET_REPOSITORY:
                key = MapperEnum.CODE
                break;
              default:
                key = MapperEnum.DESCRIPTION
            }
            return masterData[key] === data
          }
        )?.code,
    );
  }

  downloadAllSearchResults(
    filename: string = "SR search result",
    contentType: string = "text/xml",
  ) {
    const {filter, sort} = this.tableOptions.getValue();
    this.http
      .post(
        URL_POST_ALL_SEARCH_RESULTS,
        {filter, sort},
        {responseType: "text"},
      )
      .subscribe((response) => {
        this.sharedService.downloadFile(
          response,
          contentType,
          `${filename}.xml`,
        );
      });
  }

  keyMap(
    value: string,
    filter: string | SrFilterEnum,
    key: MapperEnum,
    target: MapperEnum,
  ) {
    return this.sharedService.keyMap(value, filter, key, target);
  }

  downloadDistribution(distributionResourceId: number) {
    this.sharedService.downloadDistribution(
      URL_GET_DOWNLOAD_DISTRIBUTION_FILE(distributionResourceId),
    );
  }

  mapValues(
    filter: SrFilterEnum | string,
    value: string,
    isChipRemove = false,
  ) {
    // isChipRemove is used to reverse mapping for deleting purpose and for chips labels
    let key: MapperEnum;
    let target: MapperEnum;
    switch (filter) {
      case SrFilterEnum.ASSET_TYPE:
      case SrFilterEnum.MEDIA_TYPE:
      case SrFilterEnum.TRANSACTION_TYPE:
      case SrFilterEnum.ASSOCIATION_TYPE:
      case SrFilterEnum.TRANSACTION_COMPONENT:
      case SrFilterEnum.TRANSACTION_SUBJECT:
      case SrFilterEnum.THEME:
      case SrFilterEnum.STATUS:
        key = isChipRemove ? MapperEnum.DESCRIPTION : MapperEnum.CODE;
        target = isChipRemove ? MapperEnum.CODE : MapperEnum.DESCRIPTION;
        break;
      case SrFilterEnum.LANGUAGE:
        key = isChipRemove ? MapperEnum.NAME : MapperEnum.CODE;
        target = isChipRemove ? MapperEnum.CODE : MapperEnum.NAME;
        break;
      default:
        return value;
    }

    return this.keyMap(value, filter, key, target);
  }

  getMasterData() {
    return this.sharedService.getMasterData();
  }

  setRedirectionData(asset: RedirectionDataModel | null) {
    this.sharedService.setRedirectionData(asset);
  }

  getRedirectionData(): RedirectionDataModel {
    return this.sharedService.getRedirectionData();
  }

  searchForRelatedAssets() {
    const {data, type} = this.getRedirectionData();
    let filters: ActiveFilterModel[] = [];
    switch (type) {
      case RedirectionDataEnum.TRANSACTION_LINKED_ASSETS:
        filters = [...this.handleTransactionFilters(data as TransactionModel)];
        break;
      case RedirectionDataEnum.CONFORMANT_ASSETS:
        filters = [...this.handleConformantFilters(data as AssetModel)];
        break;
      case RedirectionDataEnum.ASSOCIATION_LINKED_ASSETS:
        filters = [...this.handleAssociationFilters(data as AssociationTableModel)];
        break;
    }
    this.activeFilters.next(filters);
    this.updateTableOptions();
  }

  private updateTableOptions() {
    const options = [];
    const set = [
      ...new Set(this.activeFilters.getValue().map((v) => v.filter)),
    ];

    set.forEach((fieldName) => {
      options.push({
        fieldName,
        fieldValues: this.activeFilters
          .getValue()
          .filter((f) => f.filter === fieldName)
          .map((f) => f.value),
      });
    });
    this.tableOptions.next({
      ...this.tableOptions.getValue(),
      filter: options,
      pageNumber: 0,
    });
    this.assetsSearch();
  }

  private mapSearchResult(response: SearchResponseModel): TableDataModel {
    const list: TableDataListModel[] = [];
    response.list.forEach((asset) =>
      list.push({
        identifier: asset.identifier,
        type: this.keyMap(
          asset?.type,
          SrFilterEnum.ASSET_TYPE,
          MapperEnum.CODE,
          MapperEnum.DESCRIPTION,
        ),
        status: this.keyMap(
          asset?.status,
          SrFilterEnum.STATUS,
          MapperEnum.CODE,
          MapperEnum.DESCRIPTION,
        ),
        name: asset?.title,
        version: asset?.version,
        isCurrentVersion: asset?.isCurrentVersion,
        publisher: asset?.publisher?.name,
        creator: asset?.creator?.name,
        lastModified: asset?.modified,
        versionedIdentifier: asset.versionedIdentifier,
        issueDate: asset?.issued,
        assetRepository: asset?.catalog?.title || '',
        distributions: this.sharedService.handleDistributions(asset),
        themes: [...new Set(asset.themes)].sort(),
        transactionTypes: [...new Set((asset.transactions?.map(transactionType => transactionType.type))?.filter(value => !!value))].sort(),
        transactionComponents: [...new Set(asset.transactions?.map(transactionType => transactionType.components)?.flat().filter(value => !!value))].sort(),
        transactionSubjects: [...new Set((asset.transactions?.map(transactionType => transactionType.subject))?.filter(value => !!value))].sort(),
      }),
    );
    return {
      count: response.count,
      list: list,
    };
  }

  private assetsSearch() {
    this.sharedService.setError(null);
    this.http
      .post<SearchResponseModel>(
        URL_POST_ASSET_SEARCH,
        {...this.tableOptions.getValue(), pageNumber: this.tableOptions.getValue().pageNumber + 1}, // BE pagination starts from 1, FE from 0
      )
      .subscribe({
        next: (response: SearchResponseModel) => {
          this.tableData.next(this.mapSearchResult(response));
          this.isFirstRun.next(false);
        },
        error: (err: HttpErrorResponse) => {
          this.sharedService.setError(err.status);
        },
      });
  }

  private setAssociatedAssetVersions(response) {
    this.associatedAssetVersions.next(response.map((value) => ({
      associationType: value.associationType,
      version: value.version,
      identifier: value.identifier
    })));
  }

  private getAssociationVersionsList(list: AssociatedVersionModel[], options: TableOptionsModel): string[] {
    const result = [];
    const activeIdentifier = options.filter.find(activeFilter => activeFilter.fieldName === SrFilterEnum.ASSOCIATION_URI)?.fieldValues[0] || "";
    const activeAssociationTypes = options.filter.find(activeFiler => activeFiler.fieldName === SrFilterEnum.ASSOCIATION_TYPE)?.fieldValues as string[] || [AssociationTypeEnum.IS_PART_OF, AssociationTypeEnum.CONFORMS_TO];
    const filteredList = activeIdentifier ? [...list.filter(value => value.identifier === activeIdentifier)] : [...list];
    filteredList.forEach(version => {
      if (activeAssociationTypes.includes(version.associationType)) {
        result.push(version.version)
      }
    })
    return [...new Set(result)].sort() || []
  }

  private handleTransactionFilters(data: TransactionModel) {
    const filters = []
    if (data['type']) {
      filters.push(
        {
          index: filters.length,
          filter: SrFilterEnum.TRANSACTION_TYPE,
          value: (data as TransactionModel).type.toUpperCase() === "QUERY" ? "QUERY" : "LCM"
        })
    }

    if (data['subject']) {
      filters.push(
        {
          index: filters.length,
          filter: SrFilterEnum.TRANSACTION_SUBJECT,
          value: (data as TransactionModel).subject.toUpperCase()
        })
    }

    if (data['title']) {
      filters.push(
        {
          index: filters.length,
          filter: SrFilterEnum.KEYWORD,
          value: (data as TransactionModel).title
        })
    }

    if (data['components']?.length) {
      data['components'].forEach((component: string) => {
        filters.push(
          {
            index: filters.length,
            filter: SrFilterEnum.TRANSACTION_COMPONENT,
            value: component
          }
        )
      })
    }
    return filters;
  }

  private handleAssociationFilters(data: AssociationTableModel) {
    const filters = [];

    filters.push(
      {
        index: 0,
        filter: SrFilterEnum.ASSOCIATION_TYPE,
        value: data.type
      },
      {
        index: 1,
        filter: SrFilterEnum.ASSOCIATED_VERSION,
        value: (data as AssociationTableModel).version
      },
      {
        index: 2,
        filter: SrFilterEnum.ASSOCIATION_URI,
        value: ENVIRONMENTAL_URL + data.identifier
      }
    )
    return filters
  }

  private handleConformantFilters(data: AssetModel) {
    const filters = [];
    filters.push(
      {
        index: 0,
        filter: SrFilterEnum.SHOW_VERSIONS,
        value: true
      },
      {
        index: 1,
        filter: SrFilterEnum.ASSOCIATION_URI,
        value: data.identifier
      },
      {
        index: 2,
        filter: SrFilterEnum.ASSOCIATED_VERSION,
        value: data.version
      }
    )
    return filters
  }
}
