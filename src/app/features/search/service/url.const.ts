import {ENVIRONMENTAL_URL} from "@shared/constants/environmental-url.const";

export const URL_POST_ASSET_SEARCH: string = `${ENVIRONMENTAL_URL}assets/search`;
export const URL_POST_ALL_SEARCH_RESULTS: string = `${ENVIRONMENTAL_URL}search-for-download`;
export const URL_GET_UNIQUE_VERSIONS = `${ENVIRONMENTAL_URL}assets/unique-versions`
export const URL_POST_UNIQUE_URIS = `${ENVIRONMENTAL_URL}assets/association/available-identifiers`
