export enum SrFilterEnum {
  KEYWORD = 'text',
  ASSET_TYPE = 'assetType',
  PUBLISHER = 'publisher',
  CREATOR = 'creator',
  MODIFIED = 'modified',
  LANGUAGE = 'language',
  MEDIA_TYPE = 'mediaType',
  STATUS = 'status',
  VERSION = 'version',
  ISSUED = 'issued',
  TRANSACTION_TYPE = 'transactionType',
  TRANSACTION_COMPONENT = 'transactionComponent',
  ASSOCIATION_URI = 'associatedAsset',
  ASSOCIATION_TYPE = 'associationType',
  ASSOCIATED_VERSION = 'associatedAssetVersion',
  THEME = 'theme',
  SHOW_VERSIONS = 'showVersions',
  SHOW_ASSOCIATIONS = 'showAssociations',
  TRANSACTION_SUBJECT = 'transactionSubject',
  ASSET_REPOSITORY = 'assetRepository',
  NONE = 'none',
}

export enum AssetTypeEnum {
  EXAMPLE = 'Example',
  SCHEMATRON = 'Schematron',
  CODELIST = "Codelist",
  SCHEMA = "Schema",
  TECHNICAL_SPECIFICATION = "Technical Specification",
  SEMANTIC_DATA_SPECIFICATION = "Semantic Data Specification",
  PROCEDURE = 'Procedure',
  REQUIREMENT = 'Requirement',
  EVIDENCE_TYPE = 'Evidence Type',
}

export enum AssetTypeCodeEnum {
  CODELIST = "CODELIST",
  EVIDENCE_TYPE = "EVIDENCE_TYPE",
  EXAMPLE = "EXAMPLE",
  PROCEDURE = "PROCEDURE",
  REQUIREMENT = "REQUIREMENT",
  SCHEMA = "SCHEMA",
  SCHEMATRON = "SCHEMATRON",
  SEMANTIC_DATA_SPECIFICATION = "SEMANTIC_DATA_SPECIFICATION",
  TECHNICAL_SPECIFICATION = "TECHNICAL_SPECIFICATION"
}