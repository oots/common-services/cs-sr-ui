import {ENVIRONMENTAL_URL} from "@shared/constants/environmental-url.const";

export const URL_GET_METADATA: string = `${ENVIRONMENTAL_URL}master-data`;
