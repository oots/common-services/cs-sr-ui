import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { URL_GET_METADATA } from "@shared/service/url.const";
import { BehaviorSubject, map, Observable } from "rxjs";
import { LanguageModel, MasterDataResponseModel, MediaTypeModel } from "@shared/models/master-data.model";
import { SrFilterEnum } from "@features/search/enums/sr.enum";
import { MapperEnum } from "@shared/enums/mapper.enum";
import { AssetModel, RedirectionDataModel, TableDistributionsModel } from "@shared/models/sr.model";

@Injectable({
  providedIn: "root",
})
export class SharedService {
  protected genericError = new BehaviorSubject<number>(null);
  protected masterData = new BehaviorSubject<MasterDataResponseModel | null>(
    null,
  );
  masterData$: Observable<MasterDataResponseModel> = this.masterData.asObservable();
  genericError$: Observable<number> = this.genericError.asObservable();
  private _redirectionData: RedirectionDataModel = null;

  constructor(private readonly http: HttpClient) {
  }

  fetchMasterData() {
    this.http.get(URL_GET_METADATA)
      .pipe(map((response: MasterDataResponseModel) => ({
            ...response,
            languages: this.mapLanguages(response.languages),
            mediaTypes: this.mapMediaTypes(response.mediaTypes),
          }
        )
      ))
      .subscribe({
        next: (response: MasterDataResponseModel) => {
          this.masterData.next(response);
        },
        error: (err: HttpErrorResponse) => {
          this.genericError.next(err.status);
        },
      });
  }

  getMasterData() {
    return this.masterData.getValue();
  }

  setError(value: number) {
    this.genericError.next(value)
  }

  keyMap(
    value: string,
    filter: string | SrFilterEnum,
    key: MapperEnum,
    target: MapperEnum,
  ) {
    return this.getMasterData()?.[
      filter.concat(filter === SrFilterEnum.STATUS ? "es" : "s")
      ].find((f) => f[key] === value)?.[target];
  }

  handleDistributions(asset: AssetModel, activeLanguage?: LanguageModel): TableDistributionsModel[] {
    const distributionsList: TableDistributionsModel[] = [];
    asset.distributions.forEach((distribution) => {
      distribution.distributionResources.forEach((resource) => {
        distributionsList.push({
          title: activeLanguage && distribution.locales.find(locale => locale.language.code === activeLanguage.code)?.title || distribution.title,
          mediaType: distribution.mediaType,
          accessUrl: resource.accessUrl || asset.landingPage,
          distributionResourceId: resource.id
        });
      });
    });
    return distributionsList;
  }

  downloadFile(data, contentType: string, filename?: string) {
    const blob = new Blob([data], {type: contentType});
    const a = document.createElement("a");

    a.href = window.URL.createObjectURL(blob);
    a.download = filename;
    document.body.appendChild(a);
    a.click();

    document.body.removeChild(a);
  }

  downloadDistribution(url: string) {
    const a = document.createElement("a");
    a.href = url;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

  setRedirectionData(data: RedirectionDataModel) {
    this._redirectionData = data;
  }

  getRedirectionData(): RedirectionDataModel {
    return this._redirectionData;
  }

  private mapLanguages(languages: LanguageModel[]): LanguageModel[] {
    return languages.map(language => ({
      code: language.code,
      codeEu: language.codeEu,
      name: `${language.name} (${language.code})`
    }))
  }

  private mapMediaTypes(mediaTypes: MediaTypeModel[]): MediaTypeModel[] {
    return mediaTypes.map(mediaType => ({
      id: mediaType.id,
      type: mediaType.type,
      name: mediaType.name,
      description: `${mediaType.code} (${mediaType.description})`,
      code: mediaType.code,
    }))
  }
}
