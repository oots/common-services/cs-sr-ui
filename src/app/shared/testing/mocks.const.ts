export const MOCK_RESPONSE_ASSET = {
  "id": 495,
  "identifier": "https://sr.dev.oots.tech.ec.europa.eu/requirements/e5030a9c-31df-4971-8b4c-a6c0bd8f16fb",
  "title": "ENGLISH TITLE",
  "description": "2ACUTGPNBPDB31628",
  "landingPage": null,
  "distributions": [
    {
      "id": 17,
      "title": "DSD-ERR-C schematron distribution",
      "description": null,
      "distributionResources": [
        {
          "accessUrl": null,
          "downloadUrl": "https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/master/OOTS-EDM/sch/DSD-ERR-C.sch"
        }
      ],
      "type": null,
      "mediaType": "XML",
      "locales": [
        {
          "language": {
            "code": "da",
            "codeEu": "DAN",
            "name": "dansk"
          },
          "identifier": null,
          "title": "DSD-ERR-C DIST dansk TITLE",
          "description": "translation",
          "versionNotes": null
        },
        {
          "language": {
            "code": "fr",
            "codeEu": "FRA",
            "name": "français"
          },
          "identifier": null,
          "title": "DSD-ERR-C DIST français TITLE",
          "description": "translation2ACUTGPNBPDB31628",
          "versionNotes": null
        }
      ]
    }
  ],
  "catalog": {
    "id": 21,
    "title": "OOTS CS assets repository",
    "description": "The catalog of the OOTS Common Services assets",
    "landingPage": null,
    "homepage": null,
    "publisher": {
      "id": 7,
      "name": "Directorate-General for Digital Services",
      "description": null,
      "locales": []
    },
    "creator": null,
    "locales": [],
    "catalog": {
      "id": 12,
      "title": "OOTS Semantic Repository",
      "description": "The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System",
      "landingPage": null,
      "homepage": "https://sr.oots.tech.ec.europa.eu/sr/home",
      "publisher": {
        "id": 7,
        "name": "Directorate-General for Digital Services",
        "description": null,
        "locales": []
      },
      "creator": null,
      "locales": [],
      "catalog": null
    }
  },
  "publisher": {
    "id": 7,
    "name": "Directorate-General for Digital Services",
    "description": null,
    "locales": []
  },
  "creator": null,
  "issued": null,
  "modified": "2024-07-02T10:51:28.940175",
  "status": "PUBLISHED",
  "type": "REQUIREMENT",
  "locales": [
    {
      "language": {
        "code": "da",
        "codeEu": "DAN",
        "name": "dansk"
      },
      "identifier": null,
      "title": "dansk TITLE",
      "description": "des da",
      "versionNotes": null
    },
    {
      "language": {
        "code": "fr",
        "codeEu": "FRA",
        "name": "français"
      },
      "identifier": null,
      "title": "français TITLE",
      "description": "translation2ACUTGPNBPDB31628",
      "versionNotes": null
    }
  ],
  "version": null,
  "versionNotes": null,
  "isCurrentVersion": null,
  "previousVersionId": null,
  "previousVersion": null,
  "conformsTo": [],
  "isPartOf": [],
  "transactions": [
    {
      "id": 1,
      "identifier": "https://sr.oots.tech.ec.europa.eu/codelist/transaction/DSD-ERR",
      "title": "DSD-ERR",
      "description": "Query Error Response of the DSD",
      "type": "QUERY",
      "components": [
        "DSD"
      ],
      "subject": null,
      "locales": [
        {
          "language": {
            "code": "da",
            "codeEu": "DAN",
            "name": "dansk"
          },
          "identifier": null,
          "title": "DSD-ERR dansk TITLE",
          "description": "translation",
          "versionNotes": null
        },
        {
          "language": {
            "code": "fr",
            "codeEu": "FRA",
            "name": "français"
          },
          "identifier": null,
          "title": "DSD-ERR français TITLE",
          "description": "translation2ACUTGPNBPDB31628",
          "versionNotes": null
        }
      ]
    },
    {
      "id": 3,
      "identifier": "https://sr.oots.tech.ec.europa.eu/codelist/transaction/DSD-ERR",
      "title": "XXX-ERR",
      "description": "Query Error Response of the DSD",
      "type": "QUERY",
      "components": [
        "DSD"
      ],
      "subject": null,
      "locales": [
        {
          "language": {
            "code": "da",
            "codeEu": "DAN",
            "name": "dansk"
          },
          "identifier": null,
          "title": "XXX-ERR dansk TITLE",
          "description": "translation",
          "versionNotes": null
        },
        {
          "language": {
            "code": "fr",
            "codeEu": "FRA",
            "name": "français"
          },
          "identifier": null,
          "title": "XXX-ERR français TITLE",
          "description": "translation2ACUTGPNBPDB31628",
          "versionNotes": null
        }
      ]
    }
  ],
  "themes": []
}
