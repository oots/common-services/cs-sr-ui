import { SrFilterEnum } from "@features/search/enums/sr.enum";
import { AgentModel, LanguageModel } from "@shared/models/master-data.model";
import { AssociationTableModel } from "@features/asset/components/associations-table/association.model";
import { RedirectionDataEnum } from "@shared/enums/redirection.enum";

export interface SearchResponseModel {
  list: AssetModel[];
  count: number;
  request: TableOptionsModel;
}

export interface TableOptionsModel {
  pageNumber: number;
  pageSize: number;
  filter: FilterModel[];
  option?: unknown;
  sort: SortModel[];
}

export interface SortModel {
  fieldName: string;
  order: string;
}

export interface FilterModel {
  fieldName: SrFilterEnum;
  fieldValues: string[] | boolean[];
}

export interface AssetModel {
  catalog?: CatalogModel;
  conformsTo: AssociationModel [];
  creator?: AgentModel;
  description?: string;
  distributions?: DistributionModel[];
  id?: number;
  identifier?: string;
  isCurrentVersion?: boolean;
  isPartOf?: AssociationModel[];
  isProfileOf?: ProfileOfModel[];
  isSnapshotOf?: SnapshotOfModel[];
  issued?: string;
  landingPage?: string;
  locales?: LocaleModel[];
  modified?: string;
  previousVersion?: string;
  previousVersionId?: number;
  publisher?: AgentModel;
  referenceFrameworks?: ReferenceFrameworkModel[];
  status?: string;
  versionedIdentifier?: string;
  themes: string[];
  title?: string
  transactions: TransactionModel[];
  type?: string;
  version?: string;
  versionNotes?: string;
}

export interface CatalogModel {
  id: number;
  title: string;
  description?: string;
  publisher: AgentModel;
  creator?: AgentModel;
  locales: LocaleModel[];
  catalog?: CatalogModel;
}

export interface DistributionModel {
  id: number;
  title: string;
  description?: string;
  distributionResources: DistributionResourceModel[];
  type?: string;
  mediaType: string;
  locales: LocaleModel[];
}

export interface DistributionResourceModel {
  id: number;
  accessUrl?: string;
  downloadUrl?: string;
}

export interface LocaleModel {
  language?: LanguageModel;
  identifier?: string;
  title?: string;
  description?: string;
}

export interface TableDataModel {
  list: TableDataListModel[];
  count: number;
}

export interface TableDataListModel {
  identifier: string;
  type?: string;
  status?: string;
  name?: string;
  version?: string;
  publisher?: string;
  creator?: string;
  assetRepository?: string;
  lastModified?: string;
  versionedIdentifier?: string;
  issueDate?: string;
  isCurrentVersion?: boolean;
  distributions?: TableDistributionsModel[];
  themes: string[];
  transactionTypes: string[];
  transactionComponents: string[];
  transactionSubjects: string[];
}

export interface TableDistributionsModel {
  title: string;
  mediaType: string;
  accessUrl: string;
  distributionResourceId: number;
}

export interface ActiveFilterModel {
  index?: number;
  filter: SrFilterEnum | string;
  value: string | boolean;
}

export interface TransactionModel {
  id: number;
  identifier: string;
  title: string;
  description: string;
  type: string;
  components: string[];
  subject: string;
  locales: LocaleModel[];
}

export interface AssociationModel {
  id: number;
  identifier: string;
  description: string;
  version: string;
  title: string;
}

export interface ReferenceFrameworkModel {
  id: number;
  identifier: string;
  name: string;
  locales: LocaleModel[]
}

export interface RedirectionDataModel {
  data: AssociationTableModel | TransactionModel | AssetModel,
  type: RedirectionDataEnum
}

export interface ProfileOfModel {
  id: number;
  identifier: string;
  versionedIdentifier: string;
  title: string;
  description: string;
  type: string;
  version: string;
  landingPage: string;
  association: string;
  locales: LocaleModel[];
}

export interface SnapshotOfModel {
  id: number;
  identifier: string;
  versionedIdentifier: string;
  title: string;
  description: string;
  type: string;
  version: string;
  landingPage: string;
  association: string;
  locales: LocaleModel[];
}