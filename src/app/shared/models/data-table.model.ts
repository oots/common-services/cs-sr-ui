import {ActionEnum, ColumnEnum, ColumnIdEnum} from "@shared/enums/data-table.enum";

export interface DataTableConfigModel {
  isExpandable?: boolean;
  asyncTable?: boolean;
  isResponsive?: boolean;
  pagination?: PaginatorModel;
  showRadioButton?: boolean;
  isHoverable?: boolean;
  showTableHeaders?: boolean;
  defaultSorting?: {
    columns?: ColumnIdEnum,
    order?: "asc" | "desc"
  }
  translations: TableTranslationsModel;
  actions?: ActionModel[];
}

export interface ColumnModel {
  id: ColumnIdEnum; // used for sorting
  type: ColumnEnum; // user template
  columnLabel: string;

  isSortable?: boolean;
  isVisible?: boolean;
  truncateLength?: number; // 0 means no truncation
}

export interface TableDataModel {
  type: string;
  name: string;
  version: string;
  publisher: string;
  creator: string;
  lastModified: string;
  distributions: DistributionModel[];
}

export interface DistributionModel {

}

export interface TableTranslationsModel {
  emptyStateTitle: string;
  emptyStateSubtitle: string;
  showHideColumnsButton: string;
}

export interface ActionModel {
  type: ActionEnum;
  icon: string;
  tooltip?: string;
  isDisabled?: boolean;
}

export interface PaginatorModel {
  isHidden?: boolean;
  pageSizeOptions?: number[];
  visibilityLimit?: number; // Limit until paginator is visible
}
