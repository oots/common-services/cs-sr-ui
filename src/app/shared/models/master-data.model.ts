import { LocaleModel } from "@shared/models/sr.model";

export interface MasterDataResponseModel {
  agents: AgentModel[];
  assetRepositories: RepositoryModel[];
  assetTypes: AssetTypeModel[];
  associationTypes: AssociationTypeModel[];
  languages: LanguageModel[];
  mediaTypes: MediaTypeModel[];
  statuses: StatusModel[];
  themes: ThemeModel[],
  transactionComponents: TransactionDataModel[],
  transactionSubjects: SubjectModel[];
  transactionTypes: TransactionDataModel[]
}

export interface AssetTypeModel {
  id: number;
  type: string;
  name: string;
  description: string;
  code: string;
}

export interface MediaTypeModel {
  id: number;
  type: string;
  name: string;
  description: string;
  code: string;
}

export interface StatusModel {
  id: number;
  type: string;
  name: string;
  description: string;
  code: string;
}

export interface AgentModel {
  id: number;
  name: string;
  description: string;
  locales: LocaleModel[];
}

export interface LanguageModel {
  code: string;
  codeEu: string;
  name: string;
}

export interface AssociationTypeModel {
  code: string;
  description: string;
}

export interface TransactionDataModel {
  id: number;
  type: string;
  name: string;
  description: string;
  code: string;
}

export interface ThemeModel {
  id: number;
  type: string;
  name: string;
  description: string;
  code: string;
}

export interface SubjectModel {
  id: number;
  type: string;
  name: string;
  description: string;
  code: string;
}

export interface RepositoryModel {
  id: number;
  description: string;
  code: string;
  assetTypes: string[];
}