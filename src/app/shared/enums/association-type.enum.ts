export enum AssociationTypeEnum {
  IS_PART_OF = 'is_part_of',
  CONFORMS_TO = 'conforms_to',
  IS_PROFILE_OF = 'is_profile_of',
  IS_SNAPSHOT_OF = 'is_snapshot_of'
}
