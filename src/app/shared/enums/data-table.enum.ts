export enum ActionEnum {
  DELETE = 'DELETE',
  EDIT = 'EDIT',
  UNLINK = 'UNLINK',
}

export enum ColumnEnum {
  TEXT = "TEXT",
  LINK = "LINK",
  NUMBER = "NUMBER",
  DATE = "DATE",
  CHIP = "CHIP",
  TOGGLE = "TOGGLE",
  LIST = "LIST",
  CHIP_ARRAY = "CHIP_ARRAY"
}

export enum ColumnIdEnum {
  ASSET_TYPE = 'assetType',
  ASSET_NAME = 'assetName',
  PUBLISHER = 'publisher',
  CREATOR = 'creator',
  MODIFIED = 'modified',
  STATUS = 'status',
  VERSION = 'version',
  ISSUED = 'issued',
  THEME = 'theme',
  TRANSACTION_TYPE = 'transactionType',
  TRANSACTION_COMPONENT = 'transactionComponent',
  ASSET_REPOSITORY = 'assetRepository',
  TRANSACTION_SUBJECT = 'transactionSubject',
}
