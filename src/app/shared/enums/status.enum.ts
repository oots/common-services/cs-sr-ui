export enum StatusEnum {
  PUBLISHED = "Published",
  ARCHIVED = "Archived",
  DRAFT = "Draft",
  IN_REVIEW = "In Review",
  WITHDRAWN = "Withdrawn",
}
