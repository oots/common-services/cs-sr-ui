export const BREADCRUMBS_CONFIG = [
  {
    id: "1",
    // label: this.translateService.instant("app.components.breadcrumb.home"), todo add in the future again
    label: "Home",
    link: "home",
  },
  {
    id: "2",
    // label: this.translateService.instant("app.components.breadcrumb.sr"), todo add in the future again
    label: "Semantic Repository",
    link: "search",
  },
]
