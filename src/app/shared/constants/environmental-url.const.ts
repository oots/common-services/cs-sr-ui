import {environment} from "../../../environments/environment";

export const ENVIRONMENTAL_URL = environment.production ? `${window.location.origin}/` : environment?.endpoint['url']
