import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {BreadCrumbItem, EuiBreadcrumbComponent, EuiBreadcrumbService,} from '@eui/components/eui-breadcrumb';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrl: './breadcrumb.component.scss'
})
export class BreadcrumbComponent implements OnInit {
  @Input({required: true}) config: BreadCrumbItem[] = []
  @ViewChild('breadCrumbRef', {read: EuiBreadcrumbComponent}) breadCrumbRef: EuiBreadcrumbComponent;
  private counter = 4;

  constructor(
    protected readonly euiBreadcrumbService: EuiBreadcrumbService) {
  }

  ngOnInit() {
    this.euiBreadcrumbService.setBreadcrumb(this.config);
  }
}
