export interface DownloadPopupTranslationsModel {
  title: string;
  content: string;
  cancelButton: string;
  downloadButton: string;
  checkboxLabel: string;
}
