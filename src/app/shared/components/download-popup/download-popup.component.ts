import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {EuiDialogComponent} from "@eui/components/eui-dialog";
import {DownloadPopupTranslationsModel} from "@shared/components/download-popup/download-popup-translations.model";

@Component({
  selector: 'app-download-popup',
  templateUrl: './download-popup.component.html',
  styleUrl: './download-popup.component.scss'
})
export class DownloadPopupComponent implements OnInit {
  @Input({required: true}) sessionKey: string = '';
  @Input({required: true}) translations: DownloadPopupTranslationsModel = null;
  @Output() downloadClicked = new EventEmitter<void>()
  @ViewChild('dialog') dialog: EuiDialogComponent;
  dontShowAgain: boolean = false;

  ngOnInit() {
    this.checkSessionStorage();
  }

  onDialogClose(isDownload: boolean = false, isCancel: boolean = false) {
    if (isCancel) {
      this.dontShowAgain = false;
      this.dialog.closeDialog();
      return;
    }
    if (isDownload) {
      if (this.dontShowAgain) {
        sessionStorage.setItem(this.sessionKey, 'true');
      }
      this.downloadClicked.emit();
    }

    this.dialog.closeDialog();
  }

  onDialogOpen() {
    if (this.dontShowAgain) {
      this.downloadClicked.emit();
      return;
    }
    this.dialog.openDialog();
  }

  private checkSessionStorage() {
    this.dontShowAgain = !!sessionStorage.getItem(this.sessionKey);
  }
}
