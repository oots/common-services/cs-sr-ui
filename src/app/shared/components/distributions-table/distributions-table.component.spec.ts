import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DistributionsTableComponent } from './distributions-table.component';

describe('DistributionsTableComponent', () => {
  let component: DistributionsTableComponent;
  let fixture: ComponentFixture<DistributionsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DistributionsTableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DistributionsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
