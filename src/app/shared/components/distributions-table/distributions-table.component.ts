import {Component, EventEmitter, Input, Output} from "@angular/core";
import {TableDistributionsModel} from "@shared/models/sr.model";

@Component({
  selector: "app-assets-distributions-table",
  templateUrl: "./distributions-table.component.html",
  styleUrl: "./distributions-table.component.scss",
})
export class DistributionsTableComponent {
  @Input() distributionsData: TableDistributionsModel[] = [];
  @Input() hasLargeSizeFontsTitle: boolean = true;
  @Input() hasSorting: boolean = true;
  @Output() distributionDownloadButtonClicked = new EventEmitter<number>();

  onDistributionDownloadButtonClicked(distributionResourceId: number) {
    this.distributionDownloadButtonClicked.emit(distributionResourceId)
  }
}
