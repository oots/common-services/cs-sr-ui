import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {FormControl} from "@angular/forms";
import {TextInputConfigModel, TextInputTranslationsModel,} from "@shared/components/text-input/text-input.model";
import {debounceTime, distinctUntilChanged, takeUntil,skip} from "rxjs/operators";
import {BaseComponent} from "../../../core/components/base/base.component";

@Component({
  selector: "app-text-input",
  templateUrl: "./text-input.component.html",
  styleUrl: "./text-input.component.scss",
})
export class TextInputComponent extends BaseComponent implements OnInit {
  @Input({required: true}) id: string;
  @Input({required: true}) config: TextInputConfigModel;
  @Input({required: true}) translations: TextInputTranslationsModel;

  @Input() set defaultValue(defaultValue: string) {
    this.textInput.setValue(defaultValue);
  }

  @Input() set isDisabled(value: boolean) {
    if (value) {
      this.textInput?.disable();
      this.textInput?.setValue("");
    } else {
      this.textInput?.enable();
    }
    this._isDisabled = value;
  }

  @Output() searchButtonClicked: EventEmitter<string> =
    new EventEmitter<string>();
  @Output() textInputChanged: EventEmitter<string> = new EventEmitter<string>();

  textInput: FormControl<string> = new FormControl<string>(this.defaultValue);
  _isDisabled: boolean = false;

  ngOnInit() {
    this.textInput.valueChanges
      .pipe(
        skip(1),
        distinctUntilChanged(),
        debounceTime(300),
        takeUntil(this.destroyed),
      )
      .subscribe((value) => this.textInputChanged.emit(value?.trim()));
  }

  onSearchClicked() {
    this.searchButtonClicked.emit(this.textInput.value?.trim());
  }
}
