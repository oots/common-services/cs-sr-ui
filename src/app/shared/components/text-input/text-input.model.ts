export interface TextInputConfigModel {
  isRequired?: boolean;
  isClearable ?: boolean;
  isSearchInput?: boolean;
}

export interface TextInputTranslationsModel {
  label: string;
  placeholder: string;
}
