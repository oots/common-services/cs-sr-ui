import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrl: './autocomplete.component.scss'
})
export class AutocompleteComponent {
  @Input({required: true}) set data(data: string[]) {
    let index = 0;
    this._data = data?.map(data => ({id: index++, label: data}))
  }

  @Input() set isDisabled(value: boolean) {
    if (value) {
      this.autocompleteControl?.disable();
      this.autocompleteControl?.setValue(null);
    } else {
      this.autocompleteControl?.enable();
    }
    this._isDisabled = value;
  }

  @Input() set defaultValue(defaultValue: string) {
    this.autocompleteControl.setValue({id: 0, label: defaultValue});
  }

  @Input() placeholder: string = 'app.components.autocomplete.placeholder.default';
  @Input() title: string = '';
  @Input() hasWidePanel: boolean = false;

  @Output() inputChanged = new EventEmitter<string>();

  protected _data = []
  protected _isDisabled = false
  protected autocompleteControl = new FormControl({value: null, disabled: true})

  private _selectedVersion: string = '';

  onBlurText() {
    this.emitValue(this.autocompleteControl.value?.label || "")
  }

  onInputChange(event) {
    this.emitValue(event?.label.trim())
  }

  private emitValue(value: string) {
    if (this._selectedVersion !== value) {
      this._selectedVersion = value;
      this.inputChanged.emit(value)
    }
  }
}
