import {Component, EventEmitter, Input, Output, ViewChild, ViewEncapsulation,} from "@angular/core";
import {DateRangeConfigModel, DateRangeTranslationsModel,} from "@shared/components/date-range/date-range-config.model";
import {DateRangeModel} from "@shared/components/date-range/date-range.model";
import {EuiDateRangeSelectorComponent, EuiDateRangeSelectorDates} from "@eui/components/eui-date-range-selector";
import {FormControl} from "@angular/forms";

@Component({
  selector: "app-date-range",
  templateUrl: "./date-range.component.html",
  styleUrl: "./date-range.component.scss",
  encapsulation: ViewEncapsulation.Emulated,
})
export class DateRangeComponent {
  @Input({required: true}) config: DateRangeConfigModel;
  @Input({required: true}) translations: DateRangeTranslationsModel;

  @Input() set isDisabled(value: boolean) {
    if (value) {
      this.dateRangeControl?.disable();
    } else {
      this.dateRangeControl?.enable();
    }
    this._isDisabled = value;
  }

  @Output() selectionChanged: EventEmitter<DateRangeModel> =
    new EventEmitter<DateRangeModel>();
  @ViewChild("range") range: EuiDateRangeSelectorComponent;

  @Input() set defaultRange(defaultRange: string) {
    this.handleDefaultRange(defaultRange);
  }

  _isDisabled = false;
  dateRangeControl: FormControl<EuiDateRangeSelectorDates> = new FormControl<EuiDateRangeSelectorDates>({
    value: {
      startRange: null,
      endRange: null
    },
    disabled: false
  })

  private dateRange: { firstDate; lastDate } = {
    firstDate: null,
    lastDate: null,
  };

  protected onDateChange(date, isFirstDate: boolean = false) {
    if (date?._i.length < 10) {
      return;
    }
    this.dateRange[isFirstDate ? "firstDate" : "lastDate"] = date || null;
    this.selectionChanged.emit(this.dateRange);
  }

  private mapStringToDate(value: string): string {
    return `${value.substring(0, 4)}/${value.substring(4, 6)}/${value.substring(6, 8)}`;
  }

  private handleDefaultRange(defaultRange: string) {
    this.dateRangeControl.setValue({startRange: new Date('01/01/2000'), endRange: (new Date('01/01/2000'))})

    if (!defaultRange) {
      this.dateRangeControl.setValue({startRange: null, endRange: null})
      return;
    }

    if (defaultRange.includes("-")) {
      this.dateRangeControl.setValue({
        startRange: new Date(this.mapStringToDate(defaultRange.split("-")[0])),
        endRange: new Date(this.mapStringToDate(defaultRange.split("-")[1]))
      })
    }

    if (defaultRange.includes(">")) {
      this.dateRangeControl.setValue({
        startRange: new Date(this.mapStringToDate(defaultRange.substring(1))),
        endRange: null
      })
    }

    if (defaultRange.includes("<")) {
      this.dateRangeControl.setValue({
        startRange: null,
        endRange: new Date(this.mapStringToDate(defaultRange.substring(1)))
      })
    }

  }
}
