export interface DateRangeConfigModel {
  minDate: Date;
  maxDate: Date;
  isRequired?: boolean;
  isClearable?: boolean;
}

export interface DateRangeTranslationsModel {
  label: string;
  startDatePlaceholder: string;
  endDatePlaceholder: string;
}
