import {Moment} from "moment/moment";

export interface DateRangeModel {
  firstDate: Moment;
  lastDate: Moment;
}
