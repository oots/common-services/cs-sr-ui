import { Component, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation, } from "@angular/core";
import {
  DropdownConfigModel,
  DropdownDataModel,
  DropdownTranslationsModel
} from "@shared/components/dropdown/dropdown.model";
import { EuiTreeSelectionChanges, } from "@eui/components/eui-tree";
import { EuiDropdownComponent } from "@eui/components/eui-dropdown";
import { TreeItemModel } from "@eui/components/eui-tree/eui-tree.model";
import { TranslateService } from "@ngx-translate/core";
import { AssetTypeEnum } from "@features/search/enums/sr.enum";

@Component({
  selector: "app-dropdown",
  templateUrl: "./dropdown.component.html",
  styleUrl: "./dropdown.component.scss",
  encapsulation: ViewEncapsulation.None,
})
export class DropdownComponent implements OnInit {
  @Input({required: true}) id: string;
  @Input({required: true}) config: DropdownConfigModel;
  @Input({required: true}) translations: DropdownTranslationsModel;
  @Input() tooltipText: string = null;

  @Input({required: true}) set dropdownData(dropdownData: DropdownDataModel) {
    /*
    * The component does not support disable state natively. In order to provide this behaviour we "mock" this behavior by changing
    * the font color with secondary, and we add an icons as a prefix with the same color so the results looks like a faded out checkbox.
    * iconSvgName, iconTypeClass, and the selectable property are being included in the process.
    */
    this._dropdownData = [];
    if (dropdownData?.data?.length) {
      dropdownData?.data.forEach((data) => {
        const condition = dropdownData.disabledDataLabels?.includes(data.label);
        this._dropdownData.push({
          node: {
            treeContentBlock: {
              id: data?.id,
              label: data.label,
              iconSvgName: condition ? 'square-full:fa-regular' : null, // custom disabled state (not supported natively)
              iconTypeClass: condition ? 'neutral-lightest' : null, // custom disabled state (not supported natively)
              tooltipLabel: condition ? this.getDisableTranslation(data.label) : data.label,
            },
            isSelected: data.isVisible,
            selectable: !condition // custom disabled state (not supported natively)
          },
        } as TreeItemModel);
      });
    }
    this.activeFilters =
      dropdownData.data
        ?.filter((filter) => !!filter.isVisible)
        ?.map((filter) => filter.label) || [];
  }

  @Input() isDisabled: boolean = false;
  @Output() selectionChanged: EventEmitter<EuiTreeSelectionChanges> =
    new EventEmitter<EuiTreeSelectionChanges>();
  @ViewChild("dropdown") dropdown: EuiDropdownComponent;
  protected _dropdownData: TreeItemModel[] = [];
  protected activeFilters: string[] = [];

  constructor(private readonly translateService: TranslateService) {

  }

  ngOnInit() {
    document.querySelector(`#${this.id} .sr-custom-class > span`).classList.add("custom-flex-box");
  }

  protected onSelectionChange(event: EuiTreeSelectionChanges) {
    this.activeFilters = event.selection.map(
      (selection) => selection?.node?.treeContentBlock.label,
    );
    this.selectionChanged.emit(event);
  }

  private getDisableTranslation(label: string): string {
    let key;
    switch (label) {
      case AssetTypeEnum.EXAMPLE:
        key = 'app.components.dropdown.disabledTooltip.incompatibleWithTypeSubjectComponent';
        break;
      case AssetTypeEnum.SCHEMATRON:
        key = 'app.components.dropdown.disabledTooltip.incompatibleWithTheme';
        break;
      default:
        key = 'app.components.dropdown.disabledTooltip.generic'
    }

    return this.translateService.instant(key)
  }
}
