import {ColumnIdEnum} from "@shared/enums/data-table.enum";

export interface DropdownConfigModel {
  isMultiselect: boolean;
  display: 'flex' | 'block';
  euiColor?: 'primary' | 'secondary' | 'info' | 'success' | 'warning' | 'danger' | 'accent' | 'branding';
  isRequired?: boolean;
  showTotalInLabel?: boolean;
}

export interface DropdownTranslationsModel {
  label?: string;
  placeholder?: string;
  activeFiltersLabel?: string;
}

export interface DropdownItem {
  id?: ColumnIdEnum;
  label: string;
  isVisible: boolean;
}

export interface DropdownDataModel {
  data: DropdownItem[];
  disabledDataLabels?: string[];
}
