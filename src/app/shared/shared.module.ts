import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {EuiAllModule} from '@eui/components';
import {DateRangeComponent} from './components/date-range/date-range.component';
import {TextInputComponent} from './components/text-input/text-input.component'
import {DropdownComponent} from "@shared/components/dropdown/dropdown.component";
import {RouterModule} from '@angular/router';
import {BreadcrumbComponent} from './components/breadcrumb/breadcrumb.component';
import {DownloadPopupComponent} from './components/download-popup/download-popup.component';
import {AutocompleteComponent} from './components/autocomplete/autocomplete.component';
import {DistributionsTableComponent} from "@shared/components/distributions-table/distributions-table.component";

const MODULES = [
  FormsModule,
  RouterModule,
  ReactiveFormsModule,
  TranslateModule,
  CommonModule,
  EuiAllModule
];
const DECLARATIONS = [
  DropdownComponent,
  DateRangeComponent,
  TextInputComponent,
  BreadcrumbComponent,
  DownloadPopupComponent,
  AutocompleteComponent,
  DistributionsTableComponent
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [...DECLARATIONS],
  exports: [
    ...MODULES,
    ...DECLARATIONS
  ]
})
export class SharedModule {
}
